package com.horizzon.goinfinity.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.horizzon.goinfinity.R;

//import com.firebase.ui.auth.AuthUI;
//import com.firebase.ui.auth.IdpResponse;
//import com.google.android.gms.auth.api.Auth;
//import com.google.firebase.BuildConfig;


public class PaytmTest_First_Activity extends AppCompatActivity {
    //    Button buttonPhoneAuth;
//    private static final int RC_SIGN_IN = 101;
//
    private BottomSheetBehavior sheetBehavior;
    private LinearLayout bottom_sheet;
    private EditText editTextMobile;
    Button btn_bottom_sheet, login_button;
    Toolbar toolbar;
    ActionBar actionBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm_test__first_);


        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.login));
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayHomeAsUpEnabled(false);
//
//        buttonPhoneAuth = (Button) findViewById(R.id.buttonPhoneAuth);
//        // Set button listen
//        buttonPhoneAuth.setOnClickListener(new View.OnClickListener() {
//            @Override public void onClick(View v) {
//                doPhoneLogin();
//            }
//        });
//    }
//    private void doPhoneLogin() {
//        Intent intent = AuthUI.getInstance().createSignInIntentBuilder()
//                .setIsSmartLockEnabled(!BuildConfig.DEBUG)
//                .setAvailableProviders(Collections.singletonList(
//                        new AuthUI.IdpConfig.PhoneBuilder().build()))
//                .setLogo(R.mipmap.ic_launcher)
//                .build();
//        startActivityForResult(intent, RC_SIGN_IN);
//
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (requestCode == RC_SIGN_IN) {
//            IdpResponse idpResponse = IdpResponse.fromResultIntent(data);
//            if (resultCode == RESULT_OK) {
//                // Successfully signed in
//                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
//                showAlertDialog(user);
//            } else {
//                /**
//                 *   Sign in failed. If response is null the user canceled the
//                 *   sign-in flow using the back button. Otherwise check
//                 *   response.getError().getErrorCode() and handle the error.
//                 */
//                Toast.makeText(getBaseContext(), "Phone Auth Failed", Toast.LENGTH_LONG).show();
//            }
//        }
//    }
//    public void showAlertDialog(FirebaseUser user) {
//        AlertDialog.Builder mAlertDialog = new AlertDialog.Builder(
//                PaytmTest_First_Activity.this);
//        // Set Title
//        mAlertDialog.setTitle("Successfully Signed In");
//        // Set Message
//        mAlertDialog.setMessage(" Phone Number is " + user.getPhoneNumber());
//        mAlertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });
//        mAlertDialog.create();
//        // Showing Alert Message
//        mAlertDialog.show();
//    }


        btn_bottom_sheet = findViewById(R.id.buttonPhoneAuth);
        bottom_sheet=findViewById(R.id.bottom_sheet);
        sheetBehavior = BottomSheetBehavior.from(bottom_sheet);


        DisplayMetrics screenMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(screenMetrics);
        int screenHeight = screenMetrics.heightPixels;
        int screenWidth = screenMetrics.widthPixels;
        bottom_sheet.getLayoutParams().height = (screenHeight / 2);



// click event for show-dismiss bottom sheet
        btn_bottom_sheet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
//                    btn_bottom_sheet.setText("Close sheet");
                } else {
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                    btn_bottom_sheet.setText("Expand sheet");
//                    editTextMobile.setText("");
                }
            }
        });


        editTextMobile = bottom_sheet.findViewById(R.id.editTextMobile);
        login_button = bottom_sheet.findViewById(R.id.buttonContinue);
        editTextMobile.requestFocus();

        editTextMobile.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    InputMethodManager keyboard = (InputMethodManager) PaytmTest_First_Activity.this.getSystemService(Context.INPUT_METHOD_SERVICE);
                    keyboard.hideSoftInputFromWindow(editTextMobile.getWindowToken(), 0);
                    login_button.setText("continue");
                    login_button.setTextColor(getResources().getColor(R.color.white));

                    //do here your stuff f
                    return true;
                }
                return false;
            }


        });
        editTextMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (s.length() >=10) {
                    login_button.setText("continue");
                    login_button.setTextColor(getResources().getColor(R.color.white));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        bottom_sheet.findViewById(R.id.buttonContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile = editTextMobile.getText().toString().trim();

                if (mobile.isEmpty() || mobile.length() < 10) {
                    editTextMobile.setError("Enter a valid mobile");
                    editTextMobile.requestFocus();
                    return;
                }

                Intent intent = new Intent(PaytmTest_First_Activity.this, phone_verify.class);
                intent.putExtra("mobile", mobile);
                startActivity(intent);
            }
        });


        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
//                        btn_bottom_sheet.setText("Close Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
//                        btn_bottom_sheet.setText("Expand Sheet");
                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View view, float v) {

            }
        });
    }


    @Override
    public boolean dispatchTouchEvent(MotionEvent event){
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            if (sheetBehavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {

                Rect outRect = new Rect();
                bottom_sheet.getGlobalVisibleRect(outRect);

                if(!outRect.contains((int)event.getRawX(), (int)event.getRawY()))
                    sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//                editTextMobile.setText("");
            }
        }

        return super.dispatchTouchEvent(event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }
}
