package com.horizzon.goinfinity.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.horizzon.goinfinity.R;
import com.horizzon.goinfinity.models.Paytm_response_model;
import com.horizzon.goinfinity.models.paytm_new;
import com.horizzon.goinfinity.network.APIClient;
import com.horizzon.goinfinity.utils.JSONParser;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Paytm_Test_Second_Activity extends AppCompatActivity {
    String mid = "sFxlZC14669018099228", order_id = "", cus_id = "121", channel_id = "WEB", txt_amount = "10", website = "APPSTAGING", industry_type_id = "Retail";
    String call_back_url = "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp";
    ProgressDialog progressDialog;
    PaytmPGService service;
    String url = "https://goinfinity.in/paytm/generateChecksum.php";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm__test__second_);

//        progressDialog = new ProgressDialog(this);
//        progressDialog.setTitle(getString(R.string.processing));
//        progressDialog.setMessage(getString(R.string.please_wait));
//        progressDialog.setCancelable(false);
//        progressDialog.show();
//        paytmPayment();

        final int min = 20;
        final int max = 80;
        final int random = new Random().nextInt((max - min) + 1) + min;
        order_id= String.valueOf(random);
        sendUserDetailTOServerdd dl = new sendUserDetailTOServerdd();
        dl.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }


//    private void paytmPayment() {
//        Call<paytm_new> call = APIClient.getInstance().getChecksumnew(mid, order_id, cus_id, industry_type_id, txt_amount, website, channel_id);
//        final Paytm_response_model paytm = new Paytm_response_model(
//                mid,
//                order_id,
//                cus_id,
//                txt_amount,
//                call_back_url
//
//        );
//        call.enqueue(new Callback<paytm_new>() {
//            @Override
//            public void onResponse(Call<paytm_new> call, Response<paytm_new> response) {
//                if (response.isSuccessful()) {
//                    if (progressDialog != null)
//                        progressDialog.dismiss();
//                    Log.e("checksum", String.valueOf(response.body().getCHECKSUMHASH()));
//                    initializePaytmPayment(response.body().getCHECKSUMHASH(), paytm);
//                }
//            }
//
//            @Override
//            public void onFailure(Call<paytm_new> call, Throwable t) {
//                if (progressDialog != null)
//                    progressDialog.dismiss();
//                Toast.makeText(Paytm_Test_Second_Activity.this, "network failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

//    private void verifyChecksum(String checksumhash) {
//
//        final Paytm_response_model paytm = new Paytm_response_model(
//                mid,
//                orderId,
//                custid,
//                "10.00",
//                call_back_url
//
//        );
//
//
//        Call<verifyModel>call=APIClient.getInstance().verifychecksum(mid,"121","121","Retail","10","WEBSTAGING",checksumhash);
//
//        call.enqueue(new Callback<verifyModel>() {
//            @Override
//            public void onResponse(Call<verifyModel> call, Response<verifyModel> response) {
//                if (response.isSuccessful()){
//                    if (response.body().getISCHECKSUMVALID().equalsIgnoreCase("Y")){
//                        paytm.setCHECKSUMHASH(checksumhash);
//                        if( progressDialog != null)
//                            progressDialog.dismiss();
//
//                    }
//                }
//            }
//
//            @Override
//            public void onFailure(Call<verifyModel> call, Throwable t) {
//                if( progressDialog != null)
//                    progressDialog.dismiss();
//            }
//        });
//
//
//    }

//    private void initializePaytmPayment(String s, Paytm_response_model paytm) {
//
//        //getting paytm service
//        PaytmPGService Service = PaytmPGService.getProductionService();
//
//        //use this when using for production
//        //PaytmPGService Service = PaytmPGService.getProductionService();
//        if (progressDialog != null)
//            progressDialog.dismiss();
//        //creating a hashmap and adding all the values required
//        Map<String, String> paramMap = new HashMap<>();
//        paramMap.put("MID", paytm.getMID());
//        paramMap.put("ORDER_ID", paytm.getORDERID());
//        paramMap.put("CUST_ID", paytm.getCUSTID());
//        paramMap.put("CHANNEL_ID", channel_id);
//        paramMap.put("TXN_AMOUNT", paytm.getTXNAMOUNT());
//        paramMap.put("WEBSITE", website);
//        paramMap.put("CALLBACK_URL", call_back_url);
//        paramMap.put("CHECKSUMHASH", s);
//        paramMap.put("INDUSTRY_TYPE_ID", industry_type_id);
//
//        placeOrder(paramMap);
//
//    }
//
//
//    public void placeOrder(Map<String, String> params) {
//
//        // choosing between PayTM staging and production
//        PaytmPGService pgService = PaytmPGService.getProductionService();
//
//        PaytmOrder Order = new PaytmOrder((HashMap<String, String>) params);
//
//        pgService.initialize(Order, null);
//
//        pgService.startPaymentTransaction(this, true, true,
//                new PaytmPaymentTransactionCallback() {
//                    @Override
//                    public void onTransactionResponse(Bundle inResponse) {
//                        Log.e("response", inResponse.toString());
//                    }
//
//                    @Override
//                    public void networkNotAvailable() { // If network is not
////                        Timber.e("networkNotAvailable");
////                        finish();
//                        // available, then this
//                        // method gets called.
//                        Log.e("response", "network not avialable");
//                    }
//
//                    @Override
//                    public void clientAuthenticationFailed(String inErrorMessage) {
////                        Timber.e("clientAuthenticationFailed: %s", inErrorMessage);
////                        finish();
//                        // This method gets called if client authentication
//                        // failed. // Failure may be due to following reasons //
//                        // 1. Server error or downtime. // 2. Server unable to
//                        // generate checksum or checksum response is not in
//                        // proper format. // 3. Server failed to authenticate
//                        // that client. That is value of payt_STATUS is 2. //
//                        // Error Message describes the reason for failure.
//                        Log.e("response", inErrorMessage.toString());
//                    }
//
//                    @Override
//                    public void someUIErrorOccurred(String inErrorMessage) {
//                        Log.e("ui error", inErrorMessage.toString());
//                    }
//
//                    @Override
//                    public void onErrorLoadingWebPage(int iniErrorCode,
//                                                      String inErrorMessage, String inFailingUrl) {
////                        Timber.e("onErrorLoadingWebPage: %d | %s | %s", iniErrorCode, inErrorMessage, inFailingUrl);
////                        finish();
//                        Log.e("web page error", inErrorMessage.toString());
//                    }
//
//
//                    @Override
//                    public void onBackPressedCancelTransaction() {
////                        Toast.makeText(PayTMActivity.this, "Back pressed. Transaction cancelled", Toast.LENGTH_LONG).show();
////                        finish();
//                    }
//
//                    @Override
//                    public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
//
//                    }
//
//                });
//    }


    public class sendUserDetailTOServerdd extends AsyncTask<ArrayList<String>, Void, String> {

        private ProgressDialog dialog = new ProgressDialog(Paytm_Test_Second_Activity.this);
        String CHECKSUMHASH = "";

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Please wait");
            this.dialog.show();

            // initOrderId();

        }

        protected String doInBackground(ArrayList<String>... alldata) {

            // String  url ="http://xxx.co.in/generateChecksum.php";

            JSONParser jsonParser = new JSONParser(Paytm_Test_Second_Activity.this);
            String param = "ORDER_ID=" + order_id +
                    "&MID=" + mid +
                    "&CUS_ID=" + cus_id +
                    "&CHANNEL_ID=" + channel_id + "&INDUSTRY_TYPE_ID=" + industry_type_id + "&WEBSITE=" + website + "&TXN_AMOUNT=" + txt_amount + "&CALLBACK_URL=" + call_back_url;

            JSONObject jsonObject = jsonParser.makeHttpRequest(url, "POST", param);
            Log.e("CheckSum result >>", jsonObject.toString());
            if (jsonObject != null) {
                Log.e("checksum", jsonObject.toString());
                try {

                    CHECKSUMHASH = jsonObject.has("CHECKSUMHASH") ? jsonObject.getString("CHECKSUMHASH") : "";
                    Log.e("CheckSum result >>", CHECKSUMHASH);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return CHECKSUMHASH;
        }

        @Override
        protected void onPostExecute(String result) {
            // jab run kroge to yaha checksum dekhega
            ///ab service ko call krna hai
            Log.e(" setup acc ", "  signup result  " + result);
            PaytmPGService Service = PaytmPGService.getStagingService();
            Map<String, String> paramMap = new HashMap<String, String>();

            paramMap.put("ORDER_ID", order_id);
            //MID provided by paytm

            paramMap.put("MID", mid);
            paramMap.put("CUST_ID", cus_id);
            paramMap.put("CHANNEL_ID", channel_id);
            paramMap.put("INDUSTRY_TYPE_ID", industry_type_id);
            paramMap.put("WEBSITE", website);
            paramMap.put("TXN_AMOUNT", txt_amount);
            //
            paramMap.put("CALLBACK_URL",call_back_url);
            paramMap.put("CHECKSUMHASH", CHECKSUMHASH);
            PaytmOrder Order = new PaytmOrder((HashMap<String, String>) paramMap);


            Service.initialize(Order, null);
            Service.startPaymentTransaction(Paytm_Test_Second_Activity.this, true, true, new PaytmPaymentTransactionCallback() {
                @Override
                public void someUIErrorOccurred(String inErrorMessage) {
                    // Some UI Error Occurred in Payment Gateway Activity.
                    // // This may be due to initialization of views in
                    // Payment Gateway Activity or may be due to //
                    // initialization of webview. // Error Message details
                    // the error occurred.
                }

                @Override
                public void onTransactionResponse(Bundle inResponse) {
                    Log.e("LOG", "Payment Transaction : " + inResponse);
                    String response = inResponse.getString("RESPMSG");
                    if (response.equals("Txn Successful.")) {

                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                    Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();
                }


                @Override
                public void networkNotAvailable() {
                    // If network is not
                    // available, then this
                    // method gets called.
                }

                @Override
                public void clientAuthenticationFailed(String inErrorMessage) {
                    // This method gets called if client authentication
                    // failed. // Failure may be due to following reasons //
                    // 1. Server error or downtime. // 2. Server unable to
                    // generate checksum or checksum response is not in
                    // proper format. // 3. Server failed to authenticate
                    // that client. That is value of payt_STATUS is 2. //
                    // Error Message describes the reason for failure.
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode,
                                                  String inErrorMessage, String inFailingUrl) {

                }

                // had to be added: NOTE
                @Override
                public void onBackPressedCancelTransaction() {
                    // TODO Auto-generated method stub
                }

                @Override
                public void onTransactionCancel(String inErrorMessage, Bundle inResponse) {
                    Log.e("LOG", "Payment Transaction Failed " + inErrorMessage);
                    Toast.makeText(getBaseContext(), "Payment Transaction Failed ", Toast.LENGTH_LONG).show();
                }
            });

        }

    }
}
