package com.horizzon.goinfinity.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.FileUriExposedException;
import android.os.StrictMode;
import android.view.View;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.horizzon.goinfinity.app.MyAppPrefsManager;
import com.horizzon.goinfinity.customs.CircularImageView;

import com.horizzon.goinfinity.R;

import com.horizzon.goinfinity.constant.ConstantValues;
import com.horizzon.goinfinity.customs.DialogLoader;
import com.horizzon.goinfinity.customs.LocaleHelper;
import com.horizzon.goinfinity.databases.User_Info_DB;
import com.horizzon.goinfinity.models.logindatamodel.logindatamodel;
import com.horizzon.goinfinity.models.user_model.UserData;
import com.horizzon.goinfinity.models.user_model.UserDetails;
import com.horizzon.goinfinity.network.APIClient;
import com.horizzon.goinfinity.services.MyFirebaseInstanceIDService;
import com.horizzon.goinfinity.utils.CheckPermissions;
import com.horizzon.goinfinity.utils.Utilities;
import com.horizzon.goinfinity.utils.ImagePicker;
import com.horizzon.goinfinity.utils.ValidateInputs;

import java.lang.reflect.Type;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * SignUp activity handles User's Registration
 **/

public class Signup extends AppCompatActivity {

    View parentView;
    String profileImage;
    private static final int PICK_IMAGE_ID = 360;           // the number doesn't matter

    Toolbar toolbar;
    ActionBar actionBar;
    String mobile;
    DialogLoader dialogLoader;
//
//    AdView mAdView;
    Button signupBtn;
    FrameLayout banner_adView;
    TextView signup_loginText;
    TextView service_terms, privacy_policy, refund_policy, and_text;
    CircularImageView user_photo;
    FloatingActionButton user_photo_edit_fab;
    EditText user_firstname, user_lastname, user_email, user_password, user_mobile;
    View termcondition;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    User_Info_DB userInfoDB;
    UserDetails userDetails;
    String profileImageCurrent = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
    
//        MobileAds.initialize(this, ConstantValues.ADMOBE_ID);
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        userDetails=new UserDetails();
        userInfoDB = new User_Info_DB();


        // setting Toolbar
        toolbar = (Toolbar) findViewById(R.id.myToolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.signup));
        actionBar.setDisplayHomeAsUpEnabled(false);






        // Binding Layout Views
        user_photo = (CircularImageView) findViewById(R.id.user_photo);
        user_firstname = (EditText) findViewById(R.id.user_firstname);
        user_lastname = (EditText) findViewById(R.id.user_lastname);
        user_email = (EditText) findViewById(R.id.user_email);
        user_password = (EditText) findViewById(R.id.user_password);
        user_mobile = (EditText) findViewById(R.id.user_mobile);
        signupBtn = (Button) findViewById(R.id.signupBtn);
        and_text = (TextView) findViewById(R.id.and);
        service_terms = (TextView) findViewById(R.id.service_terms);
        privacy_policy = (TextView) findViewById(R.id.privacy_policy);
        refund_policy = (TextView) findViewById(R.id.refund_policy);
        signup_loginText = (TextView) findViewById(R.id.signup_loginText);
        banner_adView = (FrameLayout) findViewById(R.id.banner_adView);
        user_photo_edit_fab = (FloatingActionButton) findViewById(R.id.user_photo_edit_fab);
        termcondition=findViewById(R.id.term);
    
//        if (ConstantValues.IS_ADMOBE_ENABLED) {
//            // Initialize Admobe
//            mAdView = new AdView(Signup.this);
//            mAdView.setAdSize(AdSize.BANNER);
//            mAdView.setAdUnitId(ConstantValues.AD_UNIT_ID_BANNER);
//            AdRequest adRequest = new AdRequest.Builder().build();
//            banner_adView.addView(mAdView);
//            mAdView.loadAd(adRequest);
//            mAdView.setAdListener(new AdListener(){
//                @Override
//                public void onAdLoaded() {
//                    super.onAdLoaded();
//                    banner_adView.setVisibility(View.VISIBLE);
//                }
//                @Override
//                public void onAdFailedToLoad(int i) {
//                    super.onAdFailedToLoad(i);
//                    banner_adView.setVisibility(View.GONE);
//                }
//            });
//        }
//

        dialogLoader = new DialogLoader(Signup.this);
    
        and_text.setText(" "+getString(R.string.and)+" ");

        if (this.getIntent().getExtras()!=null && this.getIntent().getExtras().containsKey("alldata")){
            dialogLoader.showProgressDialog();
            String carListAsString = getIntent().getStringExtra("alldata");

            Gson gson = new Gson();
            Type type = new TypeToken<List<logindatamodel>>(){}.getType();
            List<logindatamodel> carsList = gson.fromJson(carListAsString, type);
            user_photo_edit_fab.setVisibility(View.GONE);
            for (logindatamodel data : carsList){
//                Log.i("Car Data", cars.id+"-"+cars.name);

                signupBtn.setText("Save changes");
                actionBar.setTitle("Save changes");

                termcondition.setVisibility(View.GONE);

                user_firstname.setText(data.getCustomersFirstname());
                user_lastname.setText(data.getCustomersLastname());
                user_email.setText(data.getCustomersEmailAddress());
                user_mobile.setText(data.getCustomersTelephone());



             userDetails.setCustomersId(data.getCustomersId());
             userDetails.setCustomersEmailAddress(data.getCustomersEmailAddress());
             userDetails.setCustomersFirstname(data.getCustomersFirstname());
             userDetails.setCustomersLastname(data.getCustomersLastname());
             userDetails.setCustomersGender(data.getCustomersGender());
             userDetails.setCustomersPicture(data.getCustomersPicture());
             userDetails.setCustomersDob(data.getCustomersDob());
             userDetails.setCustomersTelephone(data.getCustomersTelephone());
             userDetails.setCustomersDefaultAddressId(data.getCustomersDefaultAddressId());


                if (userDetails.getCustomersPicture() != null && !userDetails.getCustomersPicture().isEmpty()){
                    profileImageCurrent = userDetails.getCustomersPicture();
                    Glide.with(Signup.this)
                            .load(ConstantValues.ECOMMERCE_URL+profileImageCurrent).asBitmap()
                            .placeholder(R.drawable.profile)
                            .error(R.drawable.profile)
                            .into(user_photo);

                }
                else {
                    profileImageCurrent = "";
                    Glide.with(Signup.this)
                            .load(R.drawable.profile).asBitmap()
                            .placeholder(R.drawable.profile)
                            .error(R.drawable.profile)
                            .into(user_photo);
                }


                dialogLoader.hideProgressDialog();
            }
        }

        if (this.getIntent().getExtras()!=null && this.getIntent().getExtras().containsKey("mobile")){


            Intent intent = getIntent();
            mobile = intent.getStringExtra("mobile");
            user_mobile.setText(mobile);
        }

    
        
        privacy_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Signup.this, android.R.style.Theme_NoTitleBar);
                View dialogView = getLayoutInflater().inflate(R.layout.dialog_webview_fullscreen, null);
                dialog.setView(dialogView);
                dialog.setCancelable(false);
    
                final ImageButton dialog_button = (ImageButton) dialogView.findViewById(R.id.dialog_button);
                final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                final WebView dialog_webView = (WebView) dialogView.findViewById(R.id.dialog_webView);
            
                dialog_title.setText(getString(R.string.privacy_policy));
            
            
                String description = ConstantValues.PRIVACY_POLICY;
                String styleSheet = "<style> " +
                                        "body{background:#ffffff; margin:0; padding:0} " +
                                        "p{color:#757575;} " +
                                        "img{display:inline; height:auto; max-width:100%;}" +
                                    "</style>";
            
                dialog_webView.setHorizontalScrollBarEnabled(false);
                dialog_webView.loadDataWithBaseURL(null, styleSheet+description, "text/html", "utf-8", null);
            
            
                final AlertDialog alertDialog = dialog.create();
    
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    alertDialog.getWindow().setStatusBarColor(ContextCompat.getColor(Signup.this, R.color.colorPrimaryDark));
                }
            
                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
            
                alertDialog.show();
            
            }
        });
    
        refund_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Signup.this, android.R.style.Theme_NoTitleBar);
                View dialogView = getLayoutInflater().inflate(R.layout.dialog_webview_fullscreen, null);
                dialog.setView(dialogView);
                dialog.setCancelable(false);
    
                final ImageButton dialog_button = (ImageButton) dialogView.findViewById(R.id.dialog_button);
                final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                final WebView dialog_webView = (WebView) dialogView.findViewById(R.id.dialog_webView);
            
                dialog_title.setText(getString(R.string.refund_policy));
            
            
                String description = ConstantValues.REFUND_POLICY;
                String styleSheet = "<style> " +
                                        "body{background:#ffffff; margin:0; padding:0} " +
                                        "p{color:#757575;} " +
                                        "img{display:inline; height:auto; max-width:100%;}" +
                                    "</style>";
            
                dialog_webView.setHorizontalScrollBarEnabled(false);
                dialog_webView.loadDataWithBaseURL(null, styleSheet+description, "text/html", "utf-8", null);
            
            
                final AlertDialog alertDialog = dialog.create();
    
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    alertDialog.getWindow().setStatusBarColor(ContextCompat.getColor(Signup.this, R.color.colorPrimaryDark));
                }
            
                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
            
                alertDialog.show();
            }
        });
    
        service_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder dialog = new AlertDialog.Builder(Signup.this, android.R.style.Theme_NoTitleBar);
                View dialogView = getLayoutInflater().inflate(R.layout.dialog_webview_fullscreen, null);
                dialog.setView(dialogView);
                dialog.setCancelable(false);
    
                final ImageButton dialog_button = (ImageButton) dialogView.findViewById(R.id.dialog_button);
                final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                final WebView dialog_webView = (WebView) dialogView.findViewById(R.id.dialog_webView);
            
                dialog_title.setText(getString(R.string.service_terms));
            
            
                String description = ConstantValues.TERMS_SERVICES;
                String styleSheet = "<style> " +
                                        "body{background:#ffffff; margin:0; padding:0} " +
                                        "p{color:#757575;} " +
                                        "img{display:inline; height:auto; max-width:100%;}" +
                                    "</style>";
            
                dialog_webView.setHorizontalScrollBarEnabled(false);
                dialog_webView.loadDataWithBaseURL(null, styleSheet+description, "text/html", "utf-8", null);
            
            
                final AlertDialog alertDialog = dialog.create();
    
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    alertDialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                    alertDialog.getWindow().setStatusBarColor(ContextCompat.getColor(Signup.this, R.color.colorPrimaryDark));
                }
            
                dialog_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        alertDialog.dismiss();
                    }
                });
            
                alertDialog.show();
            }
        });


        // Handle Click event of user_photo_edit_fab FAB
        user_photo_edit_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckPermissions.is_CAMERA_PermissionGranted()  &&  CheckPermissions.is_STORAGE_PermissionGranted()) {
                    pickImage();
                }
                else {
                    ActivityCompat.requestPermissions
                            (
                                    Signup.this,
                                    new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                    CheckPermissions.PERMISSIONS_REQUEST_CAMERA
                            );
                }
            }
        });


        // Handle Click event of signup_loginText TextView
        signup_loginText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Finish SignUpActivity to goto the LoginActivity
                finish();
                overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
            }
        });


        // Handle Click event of signupBtn Button
        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validate Login Form Inputs
                boolean isValidData = validateForm();



                if (isValidData) {
                    parentView = v;
        
                    // Proceed User Registration
                    String type=signupBtn.getText().toString();
                    if (type.equalsIgnoreCase("Register")){
                        processRegistration();

                    }else if (type.equalsIgnoreCase("Save changes")){
                        dialogLoader.showProgressDialog();
//                        updatedata();
                        saveChanges();
                    }
                }
            }
        });
    }



    //*********** Picks User Profile Image from Gallery or Camera ********//

    private void pickImage() {
        // Get Intent with Options of Image Picker Apps from the static method of ImagePicker class
        Intent chooseImageIntent = ImagePicker.getImagePickerIntent(Signup.this);

        // Start Activity with Image Picker Intent
        startActivityForResult(chooseImageIntent, PICK_IMAGE_ID);
    }
    
    
    
    //*********** Receives the result from a previous call of startActivityForResult(Intent, int) ********//
    
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            // Handle Activity Result
            if (requestCode == PICK_IMAGE_ID) {

                // Get the User Selected Image as Bitmap from the static method of ImagePicker class
                Bitmap bitmap = ImagePicker.getImageFromResult(Signup.this, resultCode, data);

                // Upload the Bitmap to ImageView
//                user_photo.setImageBitmap(bitmap);
//
//                // Get the converted Bitmap as Base64ImageString from the static method of Helper class
//                profileImage = Utilities.getBase64ImageStringFromBitmap(bitmap);

                try {

                    user_photo.setImageBitmap(bitmap);

                    // Get the converted Bitmap as Base64ImageString from the static method of Helper class
                    profileImage = Utilities.getBase64ImageStringFromBitmap(bitmap);
                }catch (FileUriExposedException e){
                    e.printStackTrace();
                }

            }
        }
    }
    
    
    
    //*********** This method is invoked for every call on requestPermissions(Activity, String[], int) ********//
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    
        if (requestCode == CheckPermissions.PERMISSIONS_REQUEST_CAMERA) {
            if (grantResults.length > 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                // The Camera and Storage Permission is granted
                pickImage();
            }
            else {
                if (ActivityCompat.shouldShowRequestPermissionRationale(Signup.this, Manifest.permission.CAMERA)) {
                    // Show Information about why you need the permission
                    AlertDialog.Builder builder = new AlertDialog.Builder(Signup.this);
                    builder.setTitle(getString(R.string.permission_camera_storage));
                    builder.setMessage(getString(R.string.permission_camera_storage_needed));
                    builder.setPositiveButton(getString(R.string.grant), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            ActivityCompat.requestPermissions
                                    (
                                            Signup.this,
                                            new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                            CheckPermissions.PERMISSIONS_REQUEST_CAMERA
                                    );
                        }
                    });
                    builder.setNegativeButton(getString(R.string.not_now), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });
                    builder.show();
                }
                else {
                    Toast.makeText(Signup.this,getString(R.string.permission_rejected), Toast.LENGTH_LONG).show();
                }
            }
        }
    }
    
    
    
    //*********** Proceed User Registration Request ********//

    private void processRegistration() {

        dialogLoader.showProgressDialog();
    
        
        Call<UserData> call = APIClient.getInstance()
                .processRegistration
                        (
                            user_firstname.getText().toString().trim(),
                            user_lastname.getText().toString().trim(),
                            user_email.getText().toString().trim(),
                            user_mobile.getText().toString().trim(),
                            profileImage
                        );

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, retrofit2.Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                // Check if the Response is successful
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        // Finish SignUpActivity to goto the LoginActivity

                        List<UserDetails>data=response.body().getData();
                        if (data.size()>0){

                            for (int i=0;i<data.size();i++){
                                userDetails=data.get(i);
                                saveChanges();
                            }
                        }

                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        String message = response.body().getMessage();
                        Snackbar.make(parentView, message, Snackbar.LENGTH_SHORT).show();
                        
                    }
                    else {
                        // Unable to get Success status
                        Toast.makeText(Signup.this, getString(R.string.unexpected_response), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    // Show the Error Message
                    Toast.makeText(Signup.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(Signup.this, "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }



    //*********** Validate SignUp Form Inputs ********//

    private boolean validateForm() {
        if (!ValidateInputs.isValidName(user_firstname.getText().toString().trim())) {
            user_firstname.setError(getString(R.string.invalid_first_name));
            return false;
        } else if (!ValidateInputs.isValidName(user_lastname.getText().toString().trim())) {
            user_lastname.setError(getString(R.string.invalid_last_name));
            return false;
        } else if (!ValidateInputs.isValidEmail(user_email.getText().toString().trim())) {
            user_email.setError(getString(R.string.invalid_email));
            return false;
        }else if (!ValidateInputs.isValidNumber(user_mobile.getText().toString().trim())) {
            user_mobile.setError(getString(R.string.invalid_contact));
            return false;
        } else {
            return true;
        }
    }
    
    
    
    //*********** Set the Base Context for the ContextWrapper ********//
    
//    @Override
//    protected void attachBaseContext(Context newBase) {
//
//        String languageCode = ConstantValues.LANGUAGE_CODE;
//        if ("".equalsIgnoreCase(languageCode))
//            languageCode = ConstantValues.LANGUAGE_CODE = "en";
//
//        super.attachBaseContext(LocaleHelper.wrapLocale(newBase, languageCode));
//    }
    
    
    
    //*********** Called when the Activity has detected the User pressed the Back key ********//
    
    @Override
    public void onBackPressed() {
        // Finish SignUpActivity to goto the LoginActivity
        finish();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
    }


    private void saveChanges(){



        if (userInfoDB.getUserData(userDetails.getCustomersId()) != null) {
            userInfoDB.updateUserData(userDetails);
        }
        else {
            userInfoDB.insertUserData(userDetails);
        }

        // Save User Data to Local Databases

        // Save necessary details in SharedPrefs
        editor = sharedPreferences.edit();
        editor.putString("userID", userDetails.getCustomersId());
        editor.putString("userEmail",user_email.getText().toString());
        editor.putString("userName", userDetails.getCustomersFirstname()+" "+userDetails.getCustomersLastname());
        editor.putString("userDefaultAddressID", userDetails.getCustomersDefaultAddressId());
        editor.putString("mobile",userDetails.getCustomersTelephone());
        editor.putBoolean("isLogged_in", true);
        editor.apply();
        editor.commit();


        // Set UserLoggedIn in MyAppPrefsManager
        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(Signup.this);
        myAppPrefsManager.setUserLoggedIn(true);

        // Set isLogged_in of ConstantValues
        ConstantValues.IS_USER_LOGGED_IN = true;
        MyFirebaseInstanceIDService.RegisterDeviceForFCM(Signup.this);


        // Navigate back to MainActivity
        Intent i = new Intent(Signup.this, MainActivity.class);
        startActivity(i);
        finish();
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_out_right);
    }


    private void updatedata(){
        dialogLoader.showProgressDialog();


        Call<UserData> call = APIClient.getInstance()
                .updateCustomerInfo
                        (
                                userDetails.getCustomersId(),
                                userDetails.getCustomersFirstname(),
                                userDetails.getCustomersLastname(),
                                user_email.getText().toString(),
                                userDetails.getCustomersDob(),
                                profileImage,
                                profileImageCurrent,
                                "12345678"
                        );

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, retrofit2.Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1") && response.body().getData() != null) {
                        // User's Info has been Updated.

                        saveChanges();
                    }

                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }
}

