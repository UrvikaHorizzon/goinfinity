package com.horizzon.goinfinity.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.google.android.material.snackbar.Snackbar;

import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.horizzon.goinfinity.R;
import com.horizzon.goinfinity.app.App;
import com.horizzon.goinfinity.models.banner_model.BannerData;
import com.horizzon.goinfinity.models.banner_model.BannerDetails;
import com.horizzon.goinfinity.models.device_model.AppSettingsDetails;
import com.horizzon.goinfinity.app.MyAppPrefsManager;
import com.horizzon.goinfinity.constant.ConstantValues;
import com.horizzon.goinfinity.network.APIClient;
import com.horizzon.goinfinity.utils.Utilities;
import com.horizzon.goinfinity.network.StartAppRequests;
import com.stripe.android.net.RequestOptions;

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;


/**
 * SplashScreen activity, appears on App Startup
 **/

public class SplashScreen extends Activity {

    View rootView;
    ProgressBar progressBar;

    MyTask myTask;
    StartAppRequests startAppRequests;
    MyAppPrefsManager myAppPrefsManager;
    List<BannerDetails> bannersList;
    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static final String TAG = SplashScreen.class.getSimpleName();
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.splash);


        Glide.with(this).load(R.drawable.splash_new).asBitmap().into(new SimpleTarget<Bitmap>() {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                Drawable drawable = new BitmapDrawable(SplashScreen.this.getResources(), resource);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    getWindow().setBackgroundDrawable(drawable);
                }
            }
        });

        bannersList = new ArrayList<>();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // Bind Layout Views
        progressBar = (ProgressBar) findViewById(R.id.splash_loadingBar);
        rootView = progressBar;
        ImageView img=(ImageView)findViewById(R.id.img);
        myAppPrefsManager=new MyAppPrefsManager(this);
        Glide.with(SplashScreen.this).load(R.drawable.splash_new).asGif().into(img);
        progressBar.setVisibility(View.VISIBLE);
        Call<BannerData> call = APIClient.getInstance()
                .getfrontBanners();

        BannerData bannerData = new BannerData();

        try {
            bannerData = call.execute().body();

            if (!bannerData.getSuccess().isEmpty()) {
                bannersList = bannerData.getData();
                BannerDetails bannerDetails =  bannersList.get(0);
                URL url = new URL(ConstantValues.ECOMMERCE_URL+bannerDetails.getImage());
                Bitmap bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                BitmapDrawable background = new BitmapDrawable(getResources(), bitmap);
                getWindow().setBackgroundDrawable(background);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // Initializing StartAppRequests and PreferencesManager
        startAppRequests = new StartAppRequests(this);
        myAppPrefsManager = new MyAppPrefsManager(this);


//        ConstantValues.IS_ADMOBE_ENABLED = true;
        ConstantValues.LANGUAGE_ID = myAppPrefsManager.getUserLanguageId();


        ConstantValues.LANGUAGE_CODE = myAppPrefsManager.getUserLanguageCode();
        ConstantValues.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        ConstantValues.IS_PUSH_NOTIFICATIONS_ENABLED = myAppPrefsManager.isPushNotificationsEnabled();
        ConstantValues.IS_LOCAL_NOTIFICATIONS_ENABLED = myAppPrefsManager.isLocalNotificationsEnabled();
//        Toast.makeText(this, ""+myAppPrefsManager.getUserLanguageId(), Toast.LENGTH_SHORT).show();
        Log.e("language_id", String.valueOf(myAppPrefsManager.getUserLanguageId()));

        // Start MyTask after 3 seconds
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                myTask = new MyTask();
                myTask.execute();
            }
        }, 3000);

    }



    //*********** Sets App configuration ********//

    private void setAppConfig() {

        AppSettingsDetails appSettingsDetails = ((App) getApplicationContext()).getAppSettingsDetails();

        if (appSettingsDetails != null) {

            ConstantValues.APP_HEADER = appSettingsDetails.getAppName();

            ConstantValues.DEFAULT_HOME_STYLE = getString(R.string.actionHome) +" "+ appSettingsDetails.getHomeStyle();
            ConstantValues.DEFAULT_CATEGORY_STYLE = getString(R.string.actionCategory) +" "+ appSettingsDetails.getCategoryStyle();

//            ConstantValues.CURRENCY_SYMBOL = appSettingsDetails.getCurrencySymbol();
            ConstantValues.CURRENCY_SYMBOL = "₹ ";
            ConstantValues.NEW_PRODUCT_DURATION = appSettingsDetails.getNewProductDuration();

            ConstantValues.IS_GOOGLE_LOGIN_ENABLED = (appSettingsDetails.getGoogleLogin() == 1);
            ConstantValues.IS_FACEBOOK_LOGIN_ENABLED = (appSettingsDetails.getFacebookLogin() == 1);
            ConstantValues.IS_ADD_TO_CART_BUTTON_ENABLED = (appSettingsDetails.getCartButton() == 1);

            ConstantValues.IS_ADMOBE_ENABLED = (appSettingsDetails.getAdmob() == 1);
            ConstantValues.ADMOBE_ID = appSettingsDetails.getAdmobId();
            ConstantValues.AD_UNIT_ID_BANNER = appSettingsDetails.getAdUnitIdBanner();
            ConstantValues.AD_UNIT_ID_INTERSTITIAL = appSettingsDetails.getAdUnitIdInterstitial();


            myAppPrefsManager.setLocalNotificationsTitle(appSettingsDetails.getNotificationTitle());
            myAppPrefsManager.setLocalNotificationsDuration(appSettingsDetails.getNotificationDuration());
            myAppPrefsManager.setLocalNotificationsDescription(appSettingsDetails.getNotificationText());

        }
        else {
            ConstantValues.APP_HEADER = getString(R.string.app_name);

            ConstantValues.CURRENCY_SYMBOL = "₹ ";
            ConstantValues.NEW_PRODUCT_DURATION = 30;
            ConstantValues.IS_ADMOBE_ENABLED = false;

            ConstantValues.IS_GOOGLE_LOGIN_ENABLED = false;
            ConstantValues.IS_FACEBOOK_LOGIN_ENABLED = false;
            ConstantValues.IS_ADD_TO_CART_BUTTON_ENABLED = true;

            ConstantValues.DEFAULT_HOME_STYLE = getString(R.string.actionHome) +" "+ 1;
            ConstantValues.DEFAULT_CATEGORY_STYLE = getString(R.string.actionCategory) +" "+ 1;



        }

    }



    /************* MyTask is Inner Class, that handles StartAppRequests on Background Thread *************/

    private class MyTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            // Check for Internet Connection from the static method of Helper class
            if (Utilities.isNetworkAvailable(SplashScreen.this)) {

                // Call the method of StartAppRequests class to process App Startup Requests
                startAppRequests.StartRequests();

                return "1";
            } else {
                return "0";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            if (result.equalsIgnoreCase("0")) {

                progressBar.setVisibility(View.GONE);

                // No Internet Connection
                Snackbar.make(rootView, getString(R.string.no_internet), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.retry), new View.OnClickListener() {

                            // Handle the Retry Button Click
                            @Override
                            public void onClick(View v) {

                                progressBar.setVisibility(View.VISIBLE);

                                // Restart MyTask after 3 seconds
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        myTask = new MyTask();
                                        myTask.execute();
                                    }
                                }, 3000);
                            }
                        })
                        .show();

            }
            else {
                setAppConfig();


                if (myAppPrefsManager.isUserLoggedIn()) {
                    // Navigate to IntroScreen
                    startActivity(new Intent(getBaseContext(),MainActivity.class));
                    finish();
                }
                else {
                    // Navigate to MainActivity
                    startActivity(new Intent(getBaseContext(),PaytmTest_First_Activity.class));
                    finish();
                }
            }
        }

    }

}




