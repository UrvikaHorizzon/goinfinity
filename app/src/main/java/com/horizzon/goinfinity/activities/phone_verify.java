package com.horizzon.goinfinity.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.gson.Gson;
import com.horizzon.goinfinity.R;
import com.horizzon.goinfinity.models.logindatamodel.logindatamodel;
import com.horizzon.goinfinity.models.logindatamodel.loginresponsemodel;
import com.horizzon.goinfinity.network.APIClient;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import java.util.List;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class phone_verify extends Activity {

    private String mVerificationId;
    String mobile;
    //The edittext to input the code
    private OtpView editTextCode;

    //firebase auth object
    private FirebaseAuth mAuth;
    private ProgressBar progressbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verfiy);

        mAuth = FirebaseAuth.getInstance();
        editTextCode = findViewById(R.id.editTextCode);
        progressbar = findViewById(R.id.progressbar);

        Intent intent = getIntent();
        mobile = intent.getStringExtra("mobile");

        sendVerificationCode(mobile);

        findViewById(R.id.back_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        //if the automatic sms detection did not work, user can also enter the code manually
        //so adding a click listener to the button
        findViewById(R.id.buttonSignIn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String code = editTextCode.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    editTextCode.setError("Enter valid code");
                    editTextCode.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });
    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+91" + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }


    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                editTextCode.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(phone_verify.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            // super.onCodeSent(s, forceResendingToken);
            mVerificationId = s;

            Log.e("verification_id", s);
            Toast.makeText(phone_verify.this, "Wait for the code ", Toast.LENGTH_SHORT).show();


        }


    };


    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);
        Log.e("verifi_code", code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(phone_verify.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity

                            progressbar.setVisibility(View.VISIBLE);
                            getUserData();


                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Invalid code entered...";
                            Toast.makeText(phone_verify.this, ""+message, Toast.LENGTH_SHORT).show();
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }


                        }
                    }
                });

    }

    private void getUserData() {
        Call<loginresponsemodel> call = APIClient.getInstance().getUserData(mobile);
        progressbar.setVisibility(View.VISIBLE);
        call.enqueue(new Callback<loginresponsemodel>() {
            @Override
            public void onResponse(Call<loginresponsemodel> call, Response<loginresponsemodel> response) {
                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        if (response.body().getMessage().equalsIgnoreCase("Information is available")) {

                            List<logindatamodel> model = response.body().getData();

                            progressbar.setVisibility(View.GONE);
                            Gson gson = new Gson();
                            String jsonCars = gson.toJson(model);
                            Intent intent = new Intent(phone_verify.this, Signup.class);
                            intent.putExtra("alldata",jsonCars);
                            startActivity(intent);
                            finish();


                        }
                    }else if(response.body().getSuccess().equalsIgnoreCase("0")){
                        Intent intent = new Intent(phone_verify.this, Signup.class);
                        intent.putExtra("mobile",mobile);
                        startActivity(intent);
                        progressbar.setVisibility(View.GONE);
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<loginresponsemodel> call, Throwable t) {
                progressbar.setVisibility(View.GONE);
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
    }
}
