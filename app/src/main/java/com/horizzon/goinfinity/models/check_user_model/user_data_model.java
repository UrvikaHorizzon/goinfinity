package com.horizzon.goinfinity.models.check_user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class user_data_model {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("usertype")
    @Expose
    private String usertype;
    @SerializedName("usertype_id")
    @Expose
    private Integer usertypeId;
    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsertype() {
        return usertype;
    }

    public void setUsertype(String usertype) {
        this.usertype = usertype;
    }

    public Integer getUsertypeId() {
        return usertypeId;
    }

    public void setUsertypeId(Integer usertypeId) {
        this.usertypeId = usertypeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
