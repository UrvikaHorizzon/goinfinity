package com.horizzon.goinfinity.models.promolist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class promo_status_model {

    @SerializedName("customers_id")
    @Expose
    private Integer customersId;
    @SerializedName("customers_firstname")
    @Expose
    private String customersFirstname;
    @SerializedName("customers_lastname")
    @Expose
    private String customersLastname;
    @SerializedName("customers_email_address")
    @Expose
    private String customersEmailAddress;
    @SerializedName("customers_telephone")
    @Expose
    private String customersTelephone;
    @SerializedName("isActive")
    @Expose
    private Integer isActive;
    @SerializedName("coupon_status")
    @Expose
    private Integer couponStatus;

    public Integer getCustomersId() {
        return customersId;
    }

    public void setCustomersId(Integer customersId) {
        this.customersId = customersId;
    }

    public String getCustomersFirstname() {
        return customersFirstname;
    }

    public void setCustomersFirstname(String customersFirstname) {
        this.customersFirstname = customersFirstname;
    }

    public String getCustomersLastname() {
        return customersLastname;
    }

    public void setCustomersLastname(String customersLastname) {
        this.customersLastname = customersLastname;
    }

    public String getCustomersEmailAddress() {
        return customersEmailAddress;
    }

    public void setCustomersEmailAddress(String customersEmailAddress) {
        this.customersEmailAddress = customersEmailAddress;
    }

    public String getCustomersTelephone() {
        return customersTelephone;
    }

    public void setCustomersTelephone(String customersTelephone) {
        this.customersTelephone = customersTelephone;
    }

    public Integer getIsActive() {
        return isActive;
    }

    public void setIsActive(Integer isActive) {
        this.isActive = isActive;
    }

    public Integer getCouponStatus() {
        return couponStatus;
    }

    public void setCouponStatus(Integer couponStatus) {
        this.couponStatus = couponStatus;
    }
}
