package com.horizzon.goinfinity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class verifyModel {


    @SerializedName("MID")
    @Expose
    private String mID;
    @SerializedName("ORDERID")
    @Expose
    private String oRDERID;
    @SerializedName("TXN_AMOUNT")
    @Expose
    private String tXNAMOUNT;
    @SerializedName("IS_CHECKSUM_VALID")
    @Expose
    private String iSCHECKSUMVALID;
    @SerializedName("TXNTYPE")
    @Expose
    private String tXNTYPE;
    @SerializedName("REFUNDAMT")
    @Expose
    private String rEFUNDAMT;

    public String getMID() {
        return mID;
    }

    public void setMID(String mID) {
        this.mID = mID;
    }

    public String getORDERID() {
        return oRDERID;
    }

    public void setORDERID(String oRDERID) {
        this.oRDERID = oRDERID;
    }

    public String getTXNAMOUNT() {
        return tXNAMOUNT;
    }

    public void setTXNAMOUNT(String tXNAMOUNT) {
        this.tXNAMOUNT = tXNAMOUNT;
    }

    public String getISCHECKSUMVALID() {
        return iSCHECKSUMVALID;
    }

    public void setISCHECKSUMVALID(String iSCHECKSUMVALID) {
        this.iSCHECKSUMVALID = iSCHECKSUMVALID;
    }

    public String getTXNTYPE() {
        return tXNTYPE;
    }

    public void setTXNTYPE(String tXNTYPE) {
        this.tXNTYPE = tXNTYPE;
    }

    public String getREFUNDAMT() {
        return rEFUNDAMT;
    }

    public void setREFUNDAMT(String rEFUNDAMT) {
        this.rEFUNDAMT = rEFUNDAMT;
    }
}
