package com.horizzon.goinfinity.models.user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.horizzon.goinfinity.models.distributorlistmdoel.Data;

import java.util.List;

public class DistributorData {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<Data> data = null;
    @SerializedName("message")
    @Expose
    private String message;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
