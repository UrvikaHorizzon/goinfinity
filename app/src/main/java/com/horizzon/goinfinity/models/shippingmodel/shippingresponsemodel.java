package com.horizzon.goinfinity.models.shippingmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class shippingresponsemodel {
    @SerializedName("shipping_id")
    @Expose
    private Integer shippingId;
    @SerializedName("shipping_amount_from")
    @Expose
    private Integer shippingAmountFrom;
    @SerializedName("shipping_amount_to")
    @Expose
    private Integer shippingAmountTo;
    @SerializedName("shipping_value")
    @Expose
    private Integer shippingValue;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getShippingId() {
        return shippingId;
    }

    public void setShippingId(Integer shippingId) {
        this.shippingId = shippingId;
    }

    public Integer getShippingAmountFrom() {
        return shippingAmountFrom;
    }

    public void setShippingAmountFrom(Integer shippingAmountFrom) {
        this.shippingAmountFrom = shippingAmountFrom;
    }

    public Integer getShippingAmountTo() {
        return shippingAmountTo;
    }

    public void setShippingAmountTo(Integer shippingAmountTo) {
        this.shippingAmountTo = shippingAmountTo;
    }

    public Integer getShippingValue() {
        return shippingValue;
    }

    public void setShippingValue(Integer shippingValue) {
        this.shippingValue = shippingValue;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}
