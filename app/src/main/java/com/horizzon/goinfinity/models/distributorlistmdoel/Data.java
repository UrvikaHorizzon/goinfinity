package com.horizzon.goinfinity.models.distributorlistmdoel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {


    @SerializedName("distributor")
    @Expose
    private String distributor;


    @SerializedName("dealer")
    @Expose
    private String dealer;
    @SerializedName("saller")
    @Expose
    private String saller;


    @SerializedName("distributor_telephone")
    @Expose
    private String distributor_telephone;



    @SerializedName("device_id")
    @Expose
    private String device_id;

    //
    @SerializedName("distributor_id")
    @Expose
    private String distributor_id;

    public String getDistributor_id() {
        return distributor_id;
    }

    public void setDistributor_id(String distributor_id) {
        this.distributor_id = distributor_id;
    }

    public String getDistributor_telephone() {
        return distributor_telephone;
    }

    public void setDistributor_telephone(String distributor_telephone) {
        this.distributor_telephone = distributor_telephone;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDistributor() {
        return distributor;
    }

    public void setDistributor(String distributor) {
        this.distributor = distributor;
    }

    public String getDealer() {
        return dealer;
    }

    public void setDealer(String dealer) {
        this.dealer = dealer;
    }

    public String getSaller() {
        return saller;
    }

    public void setSaller(String saller) {
        this.saller = saller;
    }






}
