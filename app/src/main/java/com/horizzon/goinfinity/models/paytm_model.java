package com.horizzon.goinfinity.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class paytm_model {

    @SerializedName("data")
    @Expose
    private Paytm_response_model data;
    @SerializedName("ORDER_ID")
    @Expose
    private String oRDERID;
    @SerializedName("payt_STATUS")
    @Expose
    private String paytSTATUS;



    public Paytm_response_model getData() {
        return data;
    }

    public void setData(Paytm_response_model data) {
        this.data = data;
    }

    public String getORDERID() {
        return oRDERID;
    }

    public void setORDERID(String oRDERID) {
        this.oRDERID = oRDERID;
    }

    public String getPaytSTATUS() {
        return paytSTATUS;
    }

    public void setPaytSTATUS(String paytSTATUS) {
        this.paytSTATUS = paytSTATUS;
    }
}
