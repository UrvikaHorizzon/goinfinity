package com.horizzon.goinfinity.models.logindatamodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class logindatamodel {

    @SerializedName("customers_id")
    @Expose
    private String customersId;

    @SerializedName("customers_default_address_id")
    @Expose
    private String customersDefaultAddressId;

    @SerializedName("customers_firstname")
    @Expose
    private String customersFirstname;
    @SerializedName("customers_lastname")
    @Expose
    private String customersLastname;
    @SerializedName("customers_dob")
    @Expose
    private String customersDob;
    @SerializedName("customers_email_address")
    @Expose
    private String customersEmailAddress;
    @SerializedName("customers_gender")
    @Expose
    private String customersGender;
    @SerializedName("customers_telephone")
    @Expose
    private String customersTelephone;
    @SerializedName("customers_fax")
    @Expose
    private String customersFax;
    @SerializedName("customers_picture")
    @Expose
    private String customersPicture;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("zip")
    @Expose
    private Integer zip;
    @SerializedName("country")
    @Expose
    private String country;


    public String getCustomersId() {
        return customersId;
    }

    public void setCustomersId(String customersId) {
        this.customersId = customersId;
    }

    public String getCustomersDefaultAddressId() {
        return customersDefaultAddressId;
    }

    public void setCustomersDefaultAddressId(String customersDefaultAddressId) {
        this.customersDefaultAddressId = customersDefaultAddressId;
    }

    public String getCustomersFirstname() {
        return customersFirstname;
    }

    public void setCustomersFirstname(String customersFirstname) {
        this.customersFirstname = customersFirstname;
    }

    public String getCustomersLastname() {
        return customersLastname;
    }

    public void setCustomersLastname(String customersLastname) {
        this.customersLastname = customersLastname;
    }

    public String getCustomersDob() {
        return customersDob;
    }

    public void setCustomersDob(String customersDob) {
        this.customersDob = customersDob;
    }

    public String getCustomersEmailAddress() {
        return customersEmailAddress;
    }

    public void setCustomersEmailAddress(String customersEmailAddress) {
        this.customersEmailAddress = customersEmailAddress;
    }

    public String getCustomersGender() {
        return customersGender;
    }

    public void setCustomersGender(String customersGender) {
        this.customersGender = customersGender;
    }

    public String getCustomersTelephone() {
        return customersTelephone;
    }

    public void setCustomersTelephone(String customersTelephone) {
        this.customersTelephone = customersTelephone;
    }

    public String getCustomersFax() {
        return customersFax;
    }

    public void setCustomersFax(String customersFax) {
        this.customersFax = customersFax;
    }

    public String getCustomersPicture() {
        return customersPicture;
    }

    public void setCustomersPicture(String customersPicture) {
        this.customersPicture = customersPicture;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

}
