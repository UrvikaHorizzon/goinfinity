package com.horizzon.goinfinity.models.check_user_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class checkuser_response_model {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private user_data_model data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public user_data_model getData() {
        return data;
    }

    public void setData(user_data_model data) {
        this.data = data;
    }

}
