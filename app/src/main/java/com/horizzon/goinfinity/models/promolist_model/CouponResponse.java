package com.horizzon.goinfinity.models.promolist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CouponResponse {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<CouponItem> data = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CouponItem> getData() {
        return data;
    }

    public void setData(List<CouponItem> data) {
        this.data = data;
    }

}
