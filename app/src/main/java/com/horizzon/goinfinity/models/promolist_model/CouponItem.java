package com.horizzon.goinfinity.models.promolist_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CouponItem {

    @SerializedName("coupans_id")
    @Expose
    private Integer coupansId;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("date_created")
    @Expose
    private String dateCreated;
    @SerializedName("date_modified")
    @Expose
    private String dateModified;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("discount_type")
    @Expose
    private String discountType;
    @SerializedName("amount")
    @Expose
    private Integer amount;
    @SerializedName("expiry_date")
    @Expose
    private String expiryDate;
    @SerializedName("usage_count")
    @Expose
    private Integer usageCount;
    @SerializedName("individual_use")
    @Expose
    private Integer individualUse;
    @SerializedName("product_ids")
    @Expose
    private String productIds;
    @SerializedName("exclude_product_ids")
    @Expose
    private String excludeProductIds;
    @SerializedName("usage_limit")
    @Expose
    private Integer usageLimit;
    @SerializedName("usage_limit_per_user")
    @Expose
    private Integer usageLimitPerUser;
    @SerializedName("limit_usage_to_x_items")
    @Expose
    private Object limitUsageToXItems;
    @SerializedName("free_shipping")
    @Expose
    private Integer freeShipping;
    @SerializedName("product_categories")
    @Expose
    private String productCategories;
    @SerializedName("excluded_product_categories")
    @Expose
    private String excludedProductCategories;
    @SerializedName("exclude_sale_items")
    @Expose
    private Integer excludeSaleItems;
    @SerializedName("minimum_amount")
    @Expose
    private String minimumAmount;
    @SerializedName("maximum_amount")
    @Expose
    private String maximumAmount;
    @SerializedName("email_restrictions")
    @Expose
    private String emailRestrictions;
    @SerializedName("used_by")
    @Expose
    private String usedBy;

    public Integer getCoupansId() {
        return coupansId;
    }

    public void setCoupansId(Integer coupansId) {
        this.coupansId = coupansId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getDateModified() {
        return dateModified;
    }

    public void setDateModified(String dateModified) {
        this.dateModified = dateModified;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscountType() {
        return discountType;
    }

    public void setDiscountType(String discountType) {
        this.discountType = discountType;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Integer getUsageCount() {
        return usageCount;
    }

    public void setUsageCount(Integer usageCount) {
        this.usageCount = usageCount;
    }

    public Integer getIndividualUse() {
        return individualUse;
    }

    public void setIndividualUse(Integer individualUse) {
        this.individualUse = individualUse;
    }

    public String getProductIds() {
        return productIds;
    }

    public void setProductIds(String productIds) {
        this.productIds = productIds;
    }

    public String getExcludeProductIds() {
        return excludeProductIds;
    }

    public void setExcludeProductIds(String excludeProductIds) {
        this.excludeProductIds = excludeProductIds;
    }

    public Integer getUsageLimit() {
        return usageLimit;
    }

    public void setUsageLimit(Integer usageLimit) {
        this.usageLimit = usageLimit;
    }

    public Integer getUsageLimitPerUser() {
        return usageLimitPerUser;
    }

    public void setUsageLimitPerUser(Integer usageLimitPerUser) {
        this.usageLimitPerUser = usageLimitPerUser;
    }

    public Object getLimitUsageToXItems() {
        return limitUsageToXItems;
    }

    public void setLimitUsageToXItems(Object limitUsageToXItems) {
        this.limitUsageToXItems = limitUsageToXItems;
    }

    public Integer getFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(Integer freeShipping) {
        this.freeShipping = freeShipping;
    }

    public String getProductCategories() {
        return productCategories;
    }

    public void setProductCategories(String productCategories) {
        this.productCategories = productCategories;
    }

    public String getExcludedProductCategories() {
        return excludedProductCategories;
    }

    public void setExcludedProductCategories(String excludedProductCategories) {
        this.excludedProductCategories = excludedProductCategories;
    }

    public Integer getExcludeSaleItems() {
        return excludeSaleItems;
    }

    public void setExcludeSaleItems(Integer excludeSaleItems) {
        this.excludeSaleItems = excludeSaleItems;
    }

    public String getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(String minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public String getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(String maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    public String getEmailRestrictions() {
        return emailRestrictions;
    }

    public void setEmailRestrictions(String emailRestrictions) {
        this.emailRestrictions = emailRestrictions;
    }

    public String getUsedBy() {
        return usedBy;
    }

    public void setUsedBy(String usedBy) {
        this.usedBy = usedBy;
    }
}
