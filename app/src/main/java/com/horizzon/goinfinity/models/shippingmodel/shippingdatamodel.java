package com.horizzon.goinfinity.models.shippingmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class shippingdatamodel {


    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<shippingresponsemodel> data = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<shippingresponsemodel> getData() {
        return data;
    }

    public void setData(List<shippingresponsemodel> data) {
        this.data = data;
    }
}
