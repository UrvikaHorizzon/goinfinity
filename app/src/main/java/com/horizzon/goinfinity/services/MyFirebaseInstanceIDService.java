package com.horizzon.goinfinity.services;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import com.horizzon.goinfinity.models.device_model.DeviceInfo;
import com.horizzon.goinfinity.models.user_model.UserData;
import com.horizzon.goinfinity.network.APIClient;

import retrofit2.Call;
import retrofit2.Callback;
import com.horizzon.goinfinity.utils.Utilities;


/**
 * FirebaseInstanceIdService Gets FCM instance ID token from Firebase Cloud Messaging Server
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    
    SharedPreferences sharedPreferences;
    
    
    //*********** Called whenever the Token is Generated ********//
    
    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.i("notification", "refreshedToken: " + refreshedToken);
    
    
        // Save FCM Token to sharedPreferences
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("FCM_Token", refreshedToken);
        editor.apply();
        
        
        // Register Device
        RegisterDeviceForFCM(getApplicationContext());
        
    }
    
    
    
    //*********** Register Device with FCM_Token and some other Device Info ********//
    
    public static void RegisterDeviceForFCM(final Context context) {
    
        SharedPreferences sharedPreferences = context.getSharedPreferences("UserInfo", MODE_PRIVATE);
        
        DeviceInfo device = Utilities.getDeviceInfo(context);
        String token_FCM = sharedPreferences.getString("FCM_Token", "");
        String customer_ID = sharedPreferences.getString("userID", "");
        Log.d("TAG", "RegisterDeviceForFCM: " + customer_ID);

        Log.d("TAG", "RegisterDeviceForFCM: 0 :  " + token_FCM);
        Log.d("TAG", "RegisterDeviceForFCM: 1 : " +  device.getDeviceType());
        Log.d("TAG", "RegisterDeviceForFCM: 2 : " + device.getDeviceRAM());
        Log.d("TAG", "RegisterDeviceForFCM:3 :  " + device.getDeviceProcessors());
        Log.d("TAG", "RegisterDeviceForFCM: 4 :" + device.getDeviceAndroidOS());
        Log.d("TAG", "RegisterDeviceForFCM: 5 :" + device.getDeviceLocation());
        Log.d("TAG", "RegisterDeviceForFCM: 6 :" + device.getDeviceModel());
        Log.d("TAG", "RegisterDeviceForFCM: 7 :" +device.getDeviceManufacturer());
        Log.d("TAG", "RegisterDeviceForFCM: 8 :" +device.getDeviceID());

        Call<UserData> call = APIClient.getInstance()
                .registerDeviceToFCM
                        (
                                token_FCM,
                                device.getDeviceType(),
                                device.getDeviceRAM(),
                                device.getDeviceProcessors(),
                                device.getDeviceAndroidOS(),
                                device.getDeviceLocation(),
                                device.getDeviceModel(),
                                device.getDeviceManufacturer(),
                                customer_ID
                                //device.getDeviceID()
                        );
        
        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, retrofit2.Response<UserData> response) {
                
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
    
                        Log.d("notification Success:", response.body().getMessage());
//                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Log.d("notification:", response.body().getMessage());
                        Toast.makeText(context, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    }
                }
                else {
                    Log.d("notification fail : ", response.errorBody().toString());
                }
            }
            
            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                Log.d("TAG", "onFailure: " +t);
//                Toast.makeText(context, "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
        
    }
    
}
