package com.horizzon.goinfinity.services;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.horizzon.goinfinity.R;
import com.horizzon.goinfinity.activities.SplashScreen;
import com.horizzon.goinfinity.app.App;
import com.horizzon.goinfinity.utils.NotificationHelper;


/**
 * MyFirebaseMessagingService receives notification Firebase Cloud Messaging Server
 */

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    
    
    //*********** Called when the Notification is Received ********//
    
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Intent notificationIntent = new Intent(getApplicationContext(), SplashScreen.class);
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        Log.d("TAG", "onMessageReceived: title" + remoteMessage.getData().get("title"));
        Log.d("TAG", "onMessageReceived: message" + remoteMessage.getData().get("message"));
        NotificationHelper.showNewNotification
                (getApplicationContext(), notificationIntent, remoteMessage.getData().get("title"),
                        remoteMessage.getData().get("message"));
     //   NotificationHelper.showNewNotification(App.getContext(), null, getString(R.string.thank_you), "Your order has been placed successfully!!!");
        
    }
    
}
