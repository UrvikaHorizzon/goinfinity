package com.horizzon.goinfinity.adapters;


import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import com.google.gson.JsonArray;
import com.horizzon.goinfinity.activities.MainActivity;

import com.horizzon.goinfinity.activities.PaytmTest_First_Activity;
import com.horizzon.goinfinity.models.product_model.Option;
import com.horizzon.goinfinity.models.product_model.Value;
import com.horizzon.goinfinity.network.APIClient;
import com.horizzon.goinfinity.utils.Utilities;
import com.horizzon.goinfinity.activities.Login;
import com.horizzon.goinfinity.databases.User_Recents_DB;
import com.horizzon.goinfinity.fragments.My_Cart;
import com.horizzon.goinfinity.fragments.Product_Description;
import com.horizzon.goinfinity.R;

import java.util.ArrayList;
import java.util.List;

import com.horizzon.goinfinity.constant.ConstantValues;
import com.horizzon.goinfinity.models.cart_model.CartProduct;
import com.horizzon.goinfinity.models.cart_model.CartProductAttributes;
import com.horizzon.goinfinity.models.product_model.ProductDetails;

import org.json.JSONArray;
import org.json.JSONException;

import retrofit2.Call;
import retrofit2.Callback;


/**
 * ProductAdapter is the adapter class of RecyclerView holding List of Products in All_Products and other Product relevant Classes
 **/

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MyViewHolder> {

    private Context context;
    private String customerID;
    private Boolean isGridView;
    private Boolean isHorizontal;
    LayoutInflater inflater;
    private User_Recents_DB recents_db;
    private List<ProductDetails> productList;


    public ProductAdapter(Context context, List<ProductDetails> productList, Boolean isHorizontal) {
        this.context = context;
        this.productList = productList;
        this.isHorizontal = isHorizontal;
        this.inflater = LayoutInflater.from(context);
        recents_db = new User_Recents_DB();
        customerID = this.context.getSharedPreferences("UserInfo", Context.MODE_PRIVATE).getString("userID", "");
    }



    //********** Called to Inflate a Layout from XML and then return the Holder *********//

    @Override
    public MyViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View itemView = null;
        
        // Check which Layout will be Inflated
        if (isHorizontal) {
            itemView = inflater.from(parent.getContext()).inflate(R.layout.layout_product_grid_sm, parent, false);
        }
        else {
            itemView = inflater.from(parent.getContext())
                                        .inflate(isGridView ? R.layout.layout_product_grid_lg : R.layout.layout_product_list_lg, parent, false);
        }
        

        // Return a new holder instance
        return new MyViewHolder(itemView);
    }



    //********** Called by RecyclerView to display the Data at the specified Position *********//

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        
      if (holder!=null){

          if (position != productList.size()) {

              // Get the data model based on Position
              final ProductDetails product = productList.get(position);

              // Check if the Product is already in the Cart
              if (My_Cart.checkCartHasProduct(product.getProductsId())) {
                  holder.product_checked.setVisibility(View.VISIBLE);
              } else {
                  holder.product_checked.setVisibility(View.GONE);
              }


              // Set Product Image on ImageView with Glide Library
              Glide.with(context)
                      .load(ConstantValues.ECOMMERCE_URL+product.getProductsImage())
                      .listener(new RequestListener<String, GlideDrawable>() {
                          @Override
                          public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                              holder.cover_loader.setVisibility(View.GONE);
                              return false;
                          }

                          @Override
                          public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                              holder.cover_loader.setVisibility(View.GONE);
                              return false;
                          }
                      })
                      .into(holder.product_thumbnail);


              holder.product_title.setText(product.getProductsName());
              holder.product_price_old.setPaintFlags(holder.product_price_old.getPaintFlags()| Paint.STRIKE_THRU_TEXT_FLAG);


              // Calculate the Discount on Product with static method of Helper class
              final String discount = Utilities.checkDiscount(product.getProductsPrice(), product.getDiscountPrice());

              if (discount != null) {
                  // Set Product's Price
                  holder.product_price_old.setVisibility(View.VISIBLE);
                  holder.product_price_old.setText(ConstantValues.CURRENCY_SYMBOL + product.getProductsPrice());
                  holder.product_price_new.setText(ConstantValues.CURRENCY_SYMBOL + product.getDiscountPrice());

                  holder.product_tag_new.setVisibility(View.GONE);
                  holder.product_tag_new_text.setVisibility(View.GONE);

                  // Set Discount Tag and its Text
                  holder.product_tag_discount.setVisibility(View.VISIBLE);
                  holder.product_tag_discount_text.setVisibility(View.VISIBLE);
                  holder.product_tag_discount_text.setText(discount);

              } else {

                  // Check if the Product is Newly Added with the help of static method of Helper class
                  if (Utilities.checkNewProduct(product.getProductsDateAdded())) {
                      // Set New Tag and its Text
                      holder.product_tag_new.setVisibility(View.VISIBLE);
                      holder.product_tag_new_text.setVisibility(View.VISIBLE);
                  } else {
                      holder.product_tag_new.setVisibility(View.GONE);
                      holder.product_tag_new_text.setVisibility(View.GONE);
                  }

                  // Hide Discount Text and Set Product's Price
                  holder.product_tag_discount.setVisibility(View.GONE);
                  holder.product_tag_discount_text.setVisibility(View.GONE);
                  holder.product_price_old.setVisibility(View.GONE);
                  holder.product_price_new.setText(ConstantValues.CURRENCY_SYMBOL + product.getProductsPrice());
              }

              if(!customerID.equals("")) {

                  Call<JsonArray> call = APIClient.getInstance()
                          .getPrice(customerID, String.valueOf(product.getProductsId()));
                  call.enqueue(new Callback<JsonArray>() {
                      @Override
                      public void onResponse(Call<JsonArray> call, retrofit2.Response<JsonArray> response) {
                          // Check if the Response is successful

                        try {
                            if (response.body().toString()!=null){
                                String res = response.body().toString();
                                JSONArray jsonArray = null;
                                try {
                                    jsonArray = new JSONArray(res);
                                    if (jsonArray.getJSONObject(0).getString("distributor_price") != null) {
                                        holder.product_price_new.setText(ConstantValues.CURRENCY_SYMBOL + jsonArray.getJSONObject(0).getString("distributor_price"));
                                        product.setProductsPrice(jsonArray.getJSONObject(0).getString("distributor_price"));
                                    } else {
                                        holder.product_price_new.setText(ConstantValues.CURRENCY_SYMBOL + jsonArray.getJSONObject(0).getString("products_price"));
                                        product.setProductsPrice(jsonArray.getJSONObject(0).getString("products_price"));
                                    }
                                } catch (Exception e) {
                                    try {
                                        holder.product_price_new.setText(ConstantValues.CURRENCY_SYMBOL + jsonArray.getJSONObject(0).getString("products_price"));
                                        product.setProductsPrice(jsonArray.getJSONObject(0).getString("products_price"));
                                    } catch (JSONException e1) {

                                    }
                                }
                            }

                        }catch (NullPointerException e){
                            e.printStackTrace();
                        }
                      }

                      @Override
                      public void onFailure(Call<JsonArray> call, Throwable t) {
                          Toast.makeText(context, "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
                      }
                  });

              }

              holder.product_like_btn.setOnCheckedChangeListener(null);

              // Check if Product is Liked
              if (product.getIsLiked().equalsIgnoreCase("1")) {
                  holder.product_like_btn.setChecked(true);
              } else {
                  holder.product_like_btn.setChecked(false);
              }

              holder.iv_remark.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      AlertDialog.Builder dialog = new AlertDialog.Builder(context);
                      View dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_remarks, null);
                      dialog.setView(dialogView);
                      dialog.setCancelable(true);

                      final Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                      final EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                      final TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);

                      final AlertDialog alertDialog = dialog.create();
                      alertDialog.show();

                      dialog_button.setOnClickListener(new View.OnClickListener() {
                          @Override
                          public void onClick(View v) {
                              product.setProduct_remark(dialog_input.getText().toString().trim());
                              alertDialog.dismiss();
                          }
                      });
                  }
              });


              // Handle the Click event of product_like_btn ToggleButton
              holder.product_like_btn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      // Check if the User is Authenticated
                      if (ConstantValues.IS_USER_LOGGED_IN) {


                          if(holder.product_like_btn.isChecked()) {
                              product.setIsLiked("1");
                              holder.product_like_btn.setChecked(true);

                              // Like the Product for the User with the static method of Product_Description
                              Product_Description.LikeProduct(product.getProductsId(), customerID, context, view);
                          }
                          else {
                              product.setIsLiked("0");
                              holder.product_like_btn.setChecked(false);

                              // Unlike the Product for the User with the static method of Product_Description
                              Product_Description.UnlikeProduct(product.getProductsId(), customerID, context, view);
                          }

                      } else {
                          // Keep the Like Button Unchecked
                          holder.product_like_btn.setChecked(false);

                          // Navigate to Login Activity
                          Intent i = new Intent(context, PaytmTest_First_Activity.class);
                          context.startActivity(i);
                          ((MainActivity) context).finish();
                          ((MainActivity) context).overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                      }
                  }
              });

              product.setCustomersBasketQuantity(1);

              final int[] number = {1};
              number[0] = product.getCustomersBasketQuantity();


              // Decrease Product Quantity
              holder.product_item_quantity_minusBtn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      // Check if the Quantity is greater than the minimum Quantity
                      if (number[0] > 1)
                      {
                          // Decrease Quantity by 1
                          number[0] = number[0] - 1;
                          holder.product_item_quantity.setText(""+ number[0]);

                          // Calculate Product Price with selected Quantity

                          product.setCustomersBasketQuantity(number[0]);
                      }
                  }
              });


              // Increase Product Quantity
              holder.product_item_quantity_plusBtn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {
                      // Check if the Quantity is less than the maximum or stock Quantity
                      if (number[0] < product.getProductsQuantity()) {
                          // Increase Quantity by 1
                          number[0] = number[0] + 1;
                          holder.product_item_quantity.setText(""+ number[0]);

                          // Calculate Product Price with selected Quantity
                          product.setCustomersBasketQuantity(number[0]);
                      }
                  }
              });


              // Handle the Click event of product_thumbnail ImageView
              holder.product_thumbnail.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      // Get Product Info
                      Bundle itemInfo = new Bundle();
                      itemInfo.putParcelable("productDetails", product);
                      Log.e("price_adapter", String.valueOf(product.getCustomersBasketQuantity()));
                      itemInfo.putInt("quantity", product.getCustomersBasketQuantity());

                      // Navigate to Product_Description of selected Product
                      Fragment fragment = new Product_Description();
                      fragment.setArguments(itemInfo);
                      MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                      FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.main_fragment, fragment)
                              .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                              .addToBackStack(null).commit();


                      // Add the Product to User's Recently Viewed Products
                      if (!recents_db.getUserRecents().contains(product.getProductsId())) {
                          recents_db.insertRecentItem(product.getProductsId());
                      }
                  }
              });


              // Handle the Click event of product_checked ImageView
              holder.product_checked.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {

                      // Get Product Info
                      Bundle itemInfo = new Bundle();
                      itemInfo.putParcelable("productDetails", product);
                      itemInfo.putInt("quantity", product.getCustomersBasketQuantity());
                      // Navigate to Product_Description of selected Product
                      Fragment fragment = new Product_Description();
                      fragment.setArguments(itemInfo);
                      MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                      FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                      fragmentManager.beginTransaction()
                              .replace(R.id.main_fragment, fragment)
                              .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                              .addToBackStack(null).commit();


                      // Add the Product to User's Recently Viewed Products
                      if (!recents_db.getUserRecents().contains(product.getProductsId())) {
                          recents_db.insertRecentItem(product.getProductsId());
                      }
                  }
              });


              holder.product_add_cart_btn.setOnClickListener(new View.OnClickListener() {
                  @Override
                  public void onClick(View view) {


                          Bundle itemInfo = new Bundle();
                          itemInfo.putParcelable("productDetails", product);
                          Log.e("price_adapter", String.valueOf(product.getCustomersBasketQuantity()));
                          itemInfo.putInt("quantity", product.getCustomersBasketQuantity());

                          // Navigate to Product_Description of selected Product
                          Fragment fragment = new Product_Description();
                          fragment.setArguments(itemInfo);
                          MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                          FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                          fragmentManager.beginTransaction()
                                  .replace(R.id.main_fragment, fragment)
                                  .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                  .addToBackStack(null).commit();


                          // Add the Product to User's Recently Viewed Products
                          if (!recents_db.getUserRecents().contains(product.getProductsId())) {
                              recents_db.insertRecentItem(product.getProductsId());
                          }

                  }
              });



              // Check the Button's Visibility
              if (ConstantValues.IS_ADD_TO_CART_BUTTON_ENABLED) {

                  holder.product_add_cart_btn.setVisibility(View.VISIBLE);
                  holder.product_add_cart_btn.setOnClickListener(null);

                  if (product.getProductsQuantity() < 1) {
                      holder.product_add_cart_btn.setText(context.getString(R.string.outOfStock));
                      holder.product_add_cart_btn.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_corners_button_red));
                  } else {
                      if(!isHorizontal && !isGridView) {
                          holder.product_add_cart_btn.setText("ADD");
                      } else {
                          holder.product_add_cart_btn.setText(context.getString(R.string.addToCart));
                      }
                      holder.product_add_cart_btn.setBackground(ContextCompat.getDrawable(context, R.drawable.rounded_corners_button_green));
                  }

                  holder.product_add_cart_btn.setOnClickListener(new View.OnClickListener() {
                      @Override
                      public void onClick(View view) {
                          // Get Product Info
                          Bundle itemInfo = new Bundle();
                          itemInfo.putParcelable("productDetails", product);
                          itemInfo.putInt("quantity", product.getCustomersBasketQuantity());
                          // Navigate to Product_Description of selected Product
                          Fragment fragment = new Product_Description();
                          fragment.setArguments(itemInfo);
                          MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
                          FragmentManager fragmentManager = ((MainActivity) context).getSupportFragmentManager();
                          fragmentManager.beginTransaction()
                                  .replace(R.id.main_fragment, fragment)
                                  .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                                  .addToBackStack(null).commit();


                          // Add the Product to User's Recently Viewed Products
                          if (!recents_db.getUserRecents().contains(product.getProductsId())) {
                              recents_db.insertRecentItem(product.getProductsId());
                          }
                      }
                  });

              }
              else {
                  // Make the Button Invisible
                  holder.product_add_cart_btn.setVisibility(View.GONE);
              }

          }
      }

    }



    //********** Returns the total number of items in the data set *********//

    @Override
    public int getItemCount() {
        return productList.size();
    }
    
    
    
    //********** Toggles the RecyclerView LayoutManager *********//
    
    public void toggleLayout(Boolean isGridView) {
        this.isGridView = isGridView;
    }



    /********** Custom ViewHolder provides a direct reference to each of the Views within a Data_Item *********/

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ProgressBar cover_loader;
        ImageView product_checked;
        ImageButton product_item_quantity_minusBtn, product_item_quantity_plusBtn;
        Button product_add_cart_btn;
        ToggleButton product_like_btn;
        ImageView product_thumbnail, product_tag_new, product_tag_discount, iv_remark;
        TextView product_title, product_price_old, product_price_new, product_tag_new_text, product_tag_discount_text, product_item_quantity;


        public MyViewHolder(final View itemView) {
            super(itemView);
            
            product_checked = (ImageView) itemView.findViewById(R.id.product_checked);
            cover_loader = (ProgressBar) itemView.findViewById(R.id.product_cover_loader);

            product_add_cart_btn = (Button) itemView.findViewById(R.id.product_card_Btn);
            product_like_btn = (ToggleButton) itemView.findViewById(R.id.product_like_btn);
            product_title = (TextView) itemView.findViewById(R.id.product_title);
            product_price_old = (TextView) itemView.findViewById(R.id.product_price_old);
            product_price_new = (TextView) itemView.findViewById(R.id.product_price_new);
            product_item_quantity = (TextView) itemView.findViewById(R.id.product_item_quantity);
            product_thumbnail = (ImageView) itemView.findViewById(R.id.product_cover);
            product_tag_new = (ImageView) itemView.findViewById(R.id.product_tag_new);
            product_tag_new_text = (TextView) itemView.findViewById(R.id.product_tag_new_text);
            product_tag_discount = (ImageView) itemView.findViewById(R.id.product_tag_discount);
            iv_remark = (ImageView) itemView.findViewById(R.id.iv_remark);
            product_tag_discount_text = (TextView) itemView.findViewById(R.id.product_tag_discount_text);
            product_item_quantity_plusBtn = (ImageButton) itemView.findViewById(R.id.product_item_quantity_plusBtn);
            product_item_quantity_minusBtn = (ImageButton) itemView.findViewById(R.id.product_item_quantity_minusBtn);
        }
        
    }



    //********** Adds the Product to User's Cart *********//

    private void addProductToCart(ProductDetails product) {

        CartProduct cartProduct = new CartProduct();

        double productBasePrice, productFinalPrice, attributesPrice = 0;
        List<CartProductAttributes> selectedAttributesList = new ArrayList<>();


        // Check Discount on Product with the help of static method of Helper class
        final String discount = Utilities.checkDiscount(product.getProductsPrice(), product.getDiscountPrice());

        // Get Product's Price based on Discount
        if (discount != null) {
            product.setIsSaleProduct("1");
            productBasePrice = Double.parseDouble(product.getDiscountPrice());
        } else {
            product.setIsSaleProduct("0");
            productBasePrice = Double.parseDouble(product.getProductsPrice());
        }


        // Get Default Attributes from AttributesList
        for (int i=0;  i<product.getAttributes().size();  i++) {

            CartProductAttributes productAttribute = new CartProductAttributes();

            // Get Name and First Value of current Attribute
            Option option = product.getAttributes().get(i).getOption();
            Value value = product.getAttributes().get(i).getValues().get(0);


            // Add the Attribute's Value Price to the attributePrices
            String attrPrice = value.getPricePrefix() + value.getPrice();
            attributesPrice += Double.parseDouble(attrPrice);


            // Add Value to new List
            List<Value> valuesList = new ArrayList<>();
            valuesList.add(value);


            // Set the Name and Value of Attribute
            productAttribute.setOption(option);
            productAttribute.setValues(valuesList);


            // Add current Attribute to selectedAttributesList
            selectedAttributesList.add(i, productAttribute);
        }


        // Add Attributes Price to Product's Final Price
        productFinalPrice = productBasePrice + attributesPrice;


        // Set Product's Price and Quantity
//        product.setCustomersBasketQuantity(1);
        product.setProductsPrice(String.valueOf(productBasePrice));
        product.setAttributesPrice(String.valueOf(attributesPrice));
        product.setProductsFinalPrice(String.valueOf(productFinalPrice));
        // Calculate Product Price with selected Quantity
        double price = Double.parseDouble(String.valueOf(productFinalPrice)) * product.getCustomersBasketQuantity();
        product.setTotalPrice(String.valueOf(price));

        // Set Customer's Basket Product and selected Attributes Info
        cartProduct.setCustomersBasketProduct(product);
        cartProduct.setCustomersBasketProductAttributes(selectedAttributesList);
        cartProduct.setCustomersId(Integer.parseInt(customerID));


        // Add the Product to User's Cart with the help of static method of My_Cart class
        My_Cart.AddCartItem
                (
                        cartProduct
                );


        // Recreate the OptionsMenu
        ((MainActivity) context).invalidateOptionsMenu();

    }

}

