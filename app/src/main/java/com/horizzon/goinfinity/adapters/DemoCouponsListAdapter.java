package com.horizzon.goinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import com.horizzon.goinfinity.R;

import java.util.List;

import com.horizzon.goinfinity.fragments.Checkout;
import com.horizzon.goinfinity.fragments.Product_Description;
import com.horizzon.goinfinity.models.coupons_model.CouponsData;
import com.horizzon.goinfinity.models.coupons_model.CouponsInfo;
import com.horizzon.goinfinity.models.promolist_model.CouponItem;
import com.horizzon.goinfinity.models.promolist_model.CouponResponse;


/**
 * DemoCouponsListAdapter is the adapter class of ListView holding List of DemoCoupons in Checkout
 **/

public class DemoCouponsListAdapter extends BaseAdapter {

    Context context;
    private Checkout checkout;
    private List<CouponItem> couponsList;

    private LayoutInflater layoutInflater;
    boolean hide_use_button;

    public DemoCouponsListAdapter(Context context, List<CouponItem> couponsList, Checkout checkout,boolean value) {
        this.context = context;
        this.checkout = checkout;
        this.couponsList = couponsList;
        this.hide_use_button=value;
        layoutInflater = LayoutInflater.from(context);
    }

    public DemoCouponsListAdapter(Context context, List<CouponItem> couponsList, Product_Description product_description,boolean value) {

        this.context = context;
        this.couponsList = couponsList;
        this.hide_use_button=value;
        layoutInflater = LayoutInflater.from(context);
    }


    //********** Returns the total number of items in the data set represented by this Adapter *********//
    
    @Override
    public int getCount() {
        return couponsList.size();
    }
    
    
    //********** Returns the item associated with the specified position in the data set *********//
    
    @Override
    public Object getItem(int position) {
        return couponsList.get(position);
    }
    
    
    //********** Returns the item id associated with the specified position in the list *********//
    
    @Override
    public long getItemId(int position) {
        return position;
    }
    
    
    
    //********** Returns a View that displays the data at the specified position in the data set *********//
    
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.layout_demo_coupon, parent, false);

            holder = new ViewHolder();

            holder.use_coupon = (Button) convertView.findViewById(R.id.use_coupon_btn);
            holder.coupon_code = (TextView) convertView.findViewById(R.id.coupon_code);
            holder.coupon_type = (TextView) convertView.findViewById(R.id.coupon_type);
            holder.coupon_amount = (TextView) convertView.findViewById(R.id.coupon_amount);
            holder.coupon_details = (TextView) convertView.findViewById(R.id.coupon_details);

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }



        holder.coupon_code.setText("Code : "+ couponsList.get(position).getCode());
        holder.coupon_type.setText("Type : "+ couponsList.get(position).getDiscountType());
        holder.coupon_amount.setText("Discount : "+ couponsList.get(position).getAmount());
        holder.coupon_details.setText(couponsList.get(position).getDescription());

        if (hide_use_button){
            holder.use_coupon.setVisibility(View.GONE);
        }

        holder.use_coupon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkout.setCouponCode(couponsList.get(position).getCode());

            }
        });


        return convertView;
    }
    
    
    
    /********** Custom ViewHolder provides a direct reference to each of the Views within a Data_Item *********/
    
    static class ViewHolder {
        private Button use_coupon;
        private TextView coupon_code, coupon_type, coupon_amount, coupon_details;

    }

}

