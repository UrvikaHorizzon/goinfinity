package com.horizzon.goinfinity.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.goinfinity.R;
import com.horizzon.goinfinity.models.promolist_model.CouponItem;

import java.util.List;

public class PromocodeListAdapter extends RecyclerView.Adapter<PromocodeListAdapter.MyviewHolder> {

    List<CouponItem>arrayList;
    Context context;
    itemClick click;

    public PromocodeListAdapter(FragmentActivity activity, List<CouponItem> rvlist) {
        this.context=activity;
        this.arrayList=rvlist;
    }


    @NonNull
    @Override
    public MyviewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.promolist_layout,parent,false);
        return new MyviewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyviewHolder holder, int position) {
        CouponItem data=arrayList.get(position);
        holder.description.setText(data.getDescription());
        holder.promocode.setText(data.getCode());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyviewHolder extends RecyclerView.ViewHolder {
        TextView description,promocode;

        public MyviewHolder(@NonNull View itemView) {
            super(itemView);

            description=itemView.findViewById(R.id.description);
            promocode=itemView.findViewById(R.id.promocode);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click.onItemClick(v,getAdapterPosition(),arrayList.get(getAdapterPosition()));
                }
            });
        }
    }


    public interface itemClick{
        public void onItemClick(View view, int position, CouponItem promodatamodel);
    }

    public void setClick(itemClick click) {
        this.click = click;
    }
}
