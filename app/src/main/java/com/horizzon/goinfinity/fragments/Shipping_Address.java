package com.horizzon.goinfinity.fragments;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;

import androidx.annotation.Nullable;

import com.google.android.material.snackbar.Snackbar;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.text.Editable;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import com.horizzon.goinfinity.app.GPStracker;
import com.horizzon.goinfinity.customs.DialogLoader;

import com.horizzon.goinfinity.activities.MainActivity;
import com.horizzon.goinfinity.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import com.horizzon.goinfinity.app.App;
import com.horizzon.goinfinity.models.address_model.AddressData;
import com.horizzon.goinfinity.models.address_model.AddressDetails;
import com.horizzon.goinfinity.models.address_model.Countries;
import com.horizzon.goinfinity.models.address_model.CountryDetails;
import com.horizzon.goinfinity.models.address_model.ZoneDetails;
import com.horizzon.goinfinity.models.address_model.Zones;
import com.horizzon.goinfinity.models.banner_model.BannerData;
import com.horizzon.goinfinity.models.contact_model.ContactUsData;
import com.horizzon.goinfinity.models.distributorlistmdoel.Data;
import com.horizzon.goinfinity.models.user_model.DistributorData;
import com.horizzon.goinfinity.network.APIClient;
import com.horizzon.goinfinity.utils.ValidateInputs;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;


public class Shipping_Address extends Fragment implements OnGoSettings {

    View rootView;
    Boolean isUpdate = false;
    String customerID, defaultAddressID, flname;
    int selectedZoneID, selectedCountryID;

    List<String> zoneNames;
    List<String> countryNames;
    List<ZoneDetails> zoneList;
    List<CountryDetails> countryList;

    ArrayAdapter<String> zoneAdapter;
    ArrayAdapter<String> countryAdapter;

    Button proceed_checkout_btn, add_dealer_btn;
    LinearLayout default_shipping_layout;
    EditText input_firstname, input_lastname, input_address, input_country, input_zone, input_city, input_postcode;

    DialogLoader dialogLoader;
    SharedPreferences sharedpreferences;
    public static final String mypreference = "mypref";
    public static final String Address = "addressKey";
    GPStracker locationTrack;
    AddressDetails shippingAddress_new;
    private static final int ACCESS_FINE_LOCATION_INTENT_ID = 6;
    Spinner spinnerName;
    List<Data> dataList=new ArrayList<>();
    String Distributor_name,deviceId,distributorID;
    String[] data;
    private String distributor_name;
    int seller_flag = 0;
    LinearLayout ll_dealer;
    TextView sellsperson_name;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.address, container, false);

        if (getArguments() != null) {
            if (getArguments().containsKey("isUpdate")) {
                isUpdate = getArguments().getBoolean("isUpdate", false);
            }
        }

        // Set the Title of Toolbar
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.shipping_address));
        checkPermissions();
        // Get the customersID and defaultAddressID from SharedPreferences
        customerID = this.getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userID", "");

        defaultAddressID = this.getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userDefaultAddressID", "");
        locationTrack = new GPStracker(getActivity(), this);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                .permitAll().build();
        StrictMode.setThreadPolicy(policy);
//        if (locationTrack.canGetLocation()) {
//
//
//            double longitude = locationTrack.getLongitude();
//            double latitude = locationTrack.getLatitude();
//
////            Toast.makeText(getApplicationContext(), "Longitude:" + longitude + "\nLatitude:" + Double.toString(latitude), Toast.LENGTH_SHORT).show();
//        } else {
//            locationTrack.showSettingsAlert();
//        }

        // Binding Layout Views
        sellsperson_name = (TextView) rootView.findViewById(R.id.sellsperson_name);
        input_firstname = (EditText) rootView.findViewById(R.id.firstname);
        input_lastname = (EditText) rootView.findViewById(R.id.lastname);
        input_address = (EditText) rootView.findViewById(R.id.address);
        input_country = (EditText) rootView.findViewById(R.id.country);
        input_zone = (EditText) rootView.findViewById(R.id.zone);
        input_city = (EditText) rootView.findViewById(R.id.city);
        input_postcode = (EditText) rootView.findViewById(R.id.postcode);
        spinnerName = (Spinner) rootView.findViewById(R.id.spinnerName);
        default_shipping_layout = (LinearLayout) rootView.findViewById(R.id.default_shipping_layout);
        ll_dealer = (LinearLayout) rootView.findViewById(R.id.ll_dealer);
        proceed_checkout_btn = (Button) rootView.findViewById(R.id.save_address_btn);
        add_dealer_btn = (Button) rootView.findViewById(R.id.add_dealer_btn);
        sharedpreferences = getActivity().getSharedPreferences(mypreference,
                Context.MODE_PRIVATE);


        // Set KeyListener of some View to null
        input_country.setKeyListener(null);
        input_zone.setKeyListener(null);

        zoneNames = new ArrayList<>();
        countryNames = new ArrayList<>();


        spinnerName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                flname = data[position];
                deviceId  = dataList.get(position).getDevice_id();
                Distributor_name   =  dataList.get(position).getDistributor_telephone();
                distributorID=dataList.get(position).getDistributor_id();
                Log.d("TAG", "onItemSelected: 1 : " +Distributor_name );
                Log.d("TAG", "onItemSelected: 2 : " +distributorID );
                Log.d("TAG", "onItemSelected: 3 : " +deviceId );

                String[] splited = flname.split("\\s+");
                input_firstname.setText(splited[0]);
                try {
                    if (splited.length > 1) {
                        input_lastname.setText(splited[1]);
                    } else {
                        input_lastname.setText("");
                    }
                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Hide the Default Checkbox Layout
        default_shipping_layout.setVisibility(View.GONE);

        // Set the text of Button
        proceed_checkout_btn.setText(getContext().getString(R.string.next));


        dialogLoader = new DialogLoader(getContext());


        // Request Countries
        RequestCountries();

        Gson gson = new Gson();
        String json = sharedpreferences.getString(Address, "");

        AddressDetails shippingAddress1 = gson.fromJson(json, AddressDetails.class);
        if (shippingAddress1 != null) {
            setAddressDetail(shippingAddress1);
        }

        add_dealer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(getActivity());
                dialog.setContentView(R.layout.dlog_add_dealer);
                dialog.setTitle("Add New Dealer");
                final EditText dfirstname = (EditText) dialog.findViewById(R.id.dfirstname);
                final EditText dlastname = (EditText) dialog.findViewById(R.id.dlastname);
                Button dadd_dealer_btn = (Button) dialog.findViewById(R.id.dadd_dealer_btn);
                dadd_dealer_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String dfname = dfirstname.getText().toString();
                        String dlname = dlastname.getText().toString();



                        if (dfname.isEmpty()) {
                            dfirstname.setError("Please Enter First Name");
                        } else if (dlname.isEmpty()) {
                            dlastname.setError("Please Enter Last Name");
                        } else {
                            Call<ContactUsData> call = APIClient.getInstance().addNewDealer(dfname, dlname, customerID,distributor_name);
                            call.enqueue(new Callback<ContactUsData>() {
                                @Override
                                public void onResponse(Call<ContactUsData> call, retrofit2.Response<ContactUsData> response) {

                                    dialogLoader.hideProgressDialog();

                                    if (response.isSuccessful()) {
                                        if (response.body().getSuccess().equalsIgnoreCase("1")) {
                                            Toast.makeText(getContext(), "Dealer Added Successfully", Toast.LENGTH_SHORT).show();
                                            dialog.dismiss();
                                            callDistributorAPI(customerID);
                                        }
                                    } else {
                                        Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                                    }
                                }

                                @Override
                                public void onFailure(Call<ContactUsData> call, Throwable t) {
                                    dialogLoader.hideProgressDialog();
                                    Toast.makeText(getContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
                                }
                            });
                        }
                    }
                });
                dialog.show();
            }
        });

        // If an existing Address is being Edited
        if (isUpdate) {
            // Get the Shipping AddressDetails from AppContext that is being Edited
            AddressDetails shippingAddress = ((App) getContext().getApplicationContext()).getShippingAddress();
            setAddressDetail(shippingAddress);


        } else {
            // Request All Addresses of the User
//            RequestAllAddresses();
            if (locationTrack.canGetLocation()) {
                Geocoder geocoder;
                List<android.location.Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(locationTrack.getLatitude(), locationTrack.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0);
                    List<String> elephantList = Arrays.asList(address.split(","));
//                    address = elephantList.get(elephantList.size() - 4);
                    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String country = addresses.get(0).getCountryName();
                    String state = addresses.get(0).getAdminArea();
                    String pincode = addresses.get(0).getPostalCode();
                    Log.d("ADDRESS", address + "\n" + city + "\n" + country + "\n" + state + "\n" + pincode);

                    shippingAddress_new = new AddressDetails();

                    shippingAddress_new.setCountryName(country);
                    shippingAddress_new.setZoneName(state);
                    shippingAddress_new.setCity(city);
                    shippingAddress_new.setStreet(address);
                    shippingAddress_new.setPostcode(pincode);
                    shippingAddress_new.setZoneId(182);
                    shippingAddress_new.setCountriesId(99);

                    setAddressDetail(shippingAddress_new);
                    callDistributorAPI(customerID);
                    Log.e("customerID", customerID);
                    ;
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }

        // Handle Touch event of input_country EditText
        input_country.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    countryAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                    countryAdapter.addAll(countryNames);

                    AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list_search, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(false);

                    Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                    EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                    TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                    ListView dialog_list = (ListView) dialogView.findViewById(R.id.dialog_list);

                    dialog_title.setText(getString(R.string.country));
                    dialog_list.setVerticalScrollBarEnabled(true);
                    dialog_list.setAdapter(countryAdapter);

                    dialog_input.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                            countryAdapter.getFilter().filter(charSequence);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });


                    final AlertDialog alertDialog = dialog.create();

                    dialog_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.show();


                    dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            alertDialog.dismiss();
                            final String selectedItem = countryAdapter.getItem(position);

                            int countryID = 0;
                            input_country.setText(selectedItem);

                            if (!selectedItem.equalsIgnoreCase("Other")) {

                                for (int i = 0; i < countryList.size(); i++) {
                                    if (countryList.get(i).getCountriesName().equalsIgnoreCase(selectedItem)) {
                                        // Get the ID of selected Country
                                        countryID = countryList.get(i).getCountriesId();
                                    }
                                }

                            }

                            selectedCountryID = countryID;

                            input_zone.setText("");

                            // Request for all Zones in the selected Country
                            RequestZones(String.valueOf(selectedCountryID));
                        }
                    });

                }

                return false;
            }
        });


        // Handle Touch event of input_zone EditText
        input_zone.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_UP) {

                    zoneAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1);
                    zoneAdapter.addAll(zoneNames);

                    AlertDialog.Builder dialog = new AlertDialog.Builder(getContext());
                    View dialogView = getActivity().getLayoutInflater().inflate(R.layout.dialog_list_search, null);
                    dialog.setView(dialogView);
                    dialog.setCancelable(false);

                    Button dialog_button = (Button) dialogView.findViewById(R.id.dialog_button);
                    EditText dialog_input = (EditText) dialogView.findViewById(R.id.dialog_input);
                    TextView dialog_title = (TextView) dialogView.findViewById(R.id.dialog_title);
                    ListView dialog_list = (ListView) dialogView.findViewById(R.id.dialog_list);

                    dialog_title.setText(getString(R.string.zone));
                    dialog_list.setVerticalScrollBarEnabled(true);
                    dialog_list.setAdapter(zoneAdapter);

                    dialog_input.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                        }

                        @Override
                        public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                            zoneAdapter.getFilter().filter(charSequence);
                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                        }
                    });


                    final AlertDialog alertDialog = dialog.create();

                    dialog_button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            alertDialog.dismiss();
                        }
                    });

                    alertDialog.show();


                    dialog_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                            alertDialog.dismiss();
                            final String selectedItem = zoneAdapter.getItem(position);

                            int zoneID = 0;
                            input_zone.setText(selectedItem);

                            if (!zoneAdapter.getItem(position).equalsIgnoreCase("Other")) {

                                for (int i = 0; i < zoneList.size(); i++) {
                                    if (zoneList.get(i).getZoneName().equalsIgnoreCase(selectedItem)) {
                                        // Get the ID of selected Country
                                        zoneID = zoneList.get(i).getZoneId();
                                    }
                                }
                            }

                            selectedZoneID = zoneID;
                        }
                    });

                }

                return false;
            }
        });


        // Handle the Click event of Proceed Order Button
        proceed_checkout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Validate Address Form Inputs
                boolean isValidData = validateAddressForm();

                if (isValidData) {
                    // New Instance of AddressDetails
                    AddressDetails shippingAddress = new AddressDetails();

                    shippingAddress.setFirstname(input_firstname.getText().toString().trim());
                    shippingAddress.setLastname(input_lastname.getText().toString().trim());
                    shippingAddress.setCountryName(input_country.getText().toString().trim());
                    shippingAddress.setZoneName(input_zone.getText().toString().trim());
                    shippingAddress.setCity(input_city.getText().toString().trim());
                    shippingAddress.setStreet(input_address.getText().toString().trim());
                    shippingAddress.setPostcode(input_postcode.getText().toString().trim());
                    shippingAddress.setZoneId(selectedZoneID);
                    shippingAddress.setCountriesId(selectedCountryID);
                    shippingAddress.setDistributor_telephone(Distributor_name);
                    shippingAddress.setDevice_id(deviceId);
                    shippingAddress.setDistributor_id(distributorID);

                    // Save the AddressDetails
                    ((App) getContext().getApplicationContext()).setShippingAddress(shippingAddress);
                    SharedPreferences.Editor editor = sharedpreferences.edit();
                    Gson gson = new Gson();
                    String json = gson.toJson(shippingAddress);
                    editor.putString(Address, json);
                    editor.commit();

                    // Check if an Address is being Edited
                    if (isUpdate) {
                        // Navigate to Checkout Fragment
                        ((MainActivity) getContext()).getSupportFragmentManager().popBackStack();
                    } else {
                        // Navigate to Billing_Address Fragment
                        Fragment fragment = new Billing_Address();
                        if (seller_flag == 1) {
                            Bundle bundle = new Bundle();
                            bundle.putString("flname", flname);
                            fragment.setArguments(bundle);
                        }
                        FragmentManager fragmentManager = getFragmentManager();
                        fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                                .addToBackStack(null).commit();
                    }
                }
            }
        });


        return rootView;
    }

    private void callDistributorAPI(String customerID) {
        Log.d("TAG", "callDistributorAPI: " + customerID);
//        dialogLoader.showProgressDialog();
        Call<DistributorData> call = APIClient.getInstance()
                .getFLname(customerID);


        call.enqueue(new Callback<DistributorData>() {
            @Override
            public void onResponse(Call<DistributorData> call, Response<DistributorData> response) {
                DistributorData bannerDa;
                dialogLoader.hideProgressDialog();

                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                   dataList = response.body().getData();


                    String stringArray[];
                    stringArray = new String[dataList.size()];
                    data = new String[dataList.size()];

                    final SpannableStringBuilder sb = new SpannableStringBuilder("T.S.O.:" + "\t" + response.body().getData().get(0).getSaller());

                    final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD); // Span to make text bold
                    final StyleSpan iss = new StyleSpan(android.graphics.Typeface.ITALIC); //Span to make text italic
                    sb.setSpan(bss, 0, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE); // make first 4 characters Bold
//                    sb.setSpan(iss, 17, 6, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                    sellsperson_name.setText(sb);

                    seller_flag = 1;
                    ll_dealer.setVisibility(View.VISIBLE);
                    input_firstname.setVisibility(View.GONE);
                    input_lastname.setVisibility(View.GONE);

                    if (dataList.get(0).getDealer() != null && response.body().getData().get(0).getDealer().length() > 0) {

                        for (int i = 0; i < dataList.size(); i++) {
                            Data record = dataList.get(i);

                            if (record.getDealer()!=null){
                                data[0]=record.getDistributor();
                                distributor_name=dataList.get(0).getDistributor();
                            }
                            data[i] = record.getDealer();
                        }
                        ArrayAdapter aa = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, data) {
                            public View getView(int position, View convertView, ViewGroup parent) {
                                // Cast the spinner collapsed item (non-popup item) as a text view
                                TextView tv = (TextView) super.getView(position, convertView, parent);
                                // Set the text color of spinner item
//                            tv.setTextColor(getActivity().getResources().getColor(R.color.colorAccentRed));\
                                // Return the view
                                return tv;
                            }

                        };
                        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spinnerName.setAdapter(aa);
//                        seller_flag = 0;
//                        String str = data[0];
//                        String[] splited = str.split("\\s+");
////                        input_firstname.setText(splited[0]);
////                        input_lastname.setText(splited[1]);
                    }


                }
            }

            @Override
            public void onFailure(Call<DistributorData> call, Throwable t) {

            }
        });
    }

    private void setAddressDetail(AddressDetails shippingAddress) {
        // Set the Address details
        selectedZoneID = shippingAddress.getZoneId();
        selectedCountryID = shippingAddress.getCountriesId();
        if (shippingAddress.getFirstname() != null) {
            input_firstname.setText(shippingAddress.getFirstname());
        }
        if (shippingAddress.getLastname() != null) {
            input_lastname.setText(shippingAddress.getLastname());
        }
        input_address.setText(shippingAddress.getStreet());
        input_country.setText(shippingAddress.getCountryName());
        input_zone.setText(shippingAddress.getZoneName());
        input_city.setText(shippingAddress.getCity());
        input_postcode.setText(shippingAddress.getPostcode());

        RequestZones(String.valueOf(selectedCountryID));
    }

    /* Check Location Permission for Marshmallow Devices */
    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED)
                requestLocationPermission();
            else {
                locationTrack = new GPStracker(getActivity(), this);
                if (!locationTrack.canGetLocation()) {
                    locationTrack.showSettingsAlert();
                }
            }
        } else {
            locationTrack = new GPStracker(getActivity(), this);
            if (!locationTrack.canGetLocation()) {
                locationTrack.showSettingsAlert();
            }
        }

    }

    /*  Show Popup to access User Permission  */
    private void requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION)) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);

        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    ACCESS_FINE_LOCATION_INTENT_ID);
        }
    }


    //*********** Get Countries List from the Server ********//

    private void RequestCountries() {

        Call<Countries> call = APIClient.getInstance()
                .getCountries();

        call.enqueue(new Callback<Countries>() {
            @Override
            public void onResponse(Call<Countries> call, Response<Countries> response) {

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        countryList = response.body().getData();

                        // Add the Country Names to the countryNames List
                        for (int i = 0; i < countryList.size(); i++) {
                            countryNames.add(countryList.get(i).getCountriesName());
                        }

                        countryNames.add("Other");

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        Snackbar.make(rootView, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                    } else {
                        // Unable to get Success status
                        Snackbar.make(rootView, getString(R.string.unexpected_response), Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Countries> call, Throwable t) {
                Toast.makeText(getContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Get Zones List of the Country from the Server ********//

    private void RequestZones(String countryID) {

        Call<Zones> call = APIClient.getInstance()
                .getZones
                        (
                                countryID
                        );

        call.enqueue(new Callback<Zones>() {
            @Override
            public void onResponse(Call<Zones> call, Response<Zones> response) {

                if (response.isSuccessful()) {

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        zoneNames.clear();
                        zoneList = response.body().getData();

                        // Add the Zone Names to the zoneNames List
                        for (int i = 0; i < zoneList.size(); i++) {
                            zoneNames.add(zoneList.get(i).getZoneName());
                        }

                        zoneNames.add("Other");

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        Snackbar.make(rootView, response.body().getMessage(), Snackbar.LENGTH_LONG).show();

                    } else {
                        // Unable to get Success status
                        Snackbar.make(rootView, getString(R.string.unexpected_response), Snackbar.LENGTH_SHORT).show();
                    }
                } else {
                    Toast.makeText(getContext(), response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<Zones> call, Throwable t) {
                Toast.makeText(getContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Get the User's Default Address from all the Addresses ********//

    private void filterDefaultAddress(AddressData addressData) {

        // Get CountryList from Response
        List<AddressDetails> addressesList = addressData.getData();

        // Initialize new AddressDetails for DefaultAddress
        AddressDetails defaultAddress = new AddressDetails();


        for (int i = 0; i < addressesList.size(); i++) {
            // Check if the Current Address is User's Default Address
            if (addressesList.get(i).getAddressId() == addressesList.get(i).getDefaultAddress()) {
                // Set the Default AddressDetails
                defaultAddress = addressesList.get(i);
            }
        }


        // Set Default Address Data and Display to User
        setAddressDetail(defaultAddress);
    }


    //*********** Request List of User Addresses ********//

    public void RequestAllAddresses() {

        dialogLoader.showProgressDialog();

        Call<AddressData> call = APIClient.getInstance()
                .getAllAddress
                        (
                                customerID
                        );

        call.enqueue(new Callback<AddressData>() {
            @Override
            public void onResponse(Call<AddressData> call, retrofit2.Response<AddressData> response) {

                dialogLoader.hideProgressDialog();

                // Check if the Response is successful
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        // Filter all the Addresses to get the Default Address
                        filterDefaultAddress(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<AddressData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    //*********** Validate Address Form Inputs ********//

    private boolean validateAddressForm() {
        if (!ValidateInputs.isValidName(input_firstname.getText().toString().trim())) {
            input_firstname.setError(getString(R.string.invalid_first_name));
            return false;
        } else if (!ValidateInputs.isValidName(input_lastname.getText().toString().trim()) && seller_flag == 0) {
            input_lastname.setError(getString(R.string.invalid_last_name));
            return false;
        } else if (!ValidateInputs.isValidInput(input_address.getText().toString().trim())) {
            input_address.setError(getString(R.string.invalid_address));
            return false;
        } else if (!ValidateInputs.isValidInput(input_country.getText().toString().trim())) {
            input_country.setError(getString(R.string.select_country));
            return false;
        } else if (!ValidateInputs.isValidInput(input_zone.getText().toString().trim())) {
            input_zone.setError(getString(R.string.select_zone));
            return false;
        } else if (!ValidateInputs.isValidInput(input_city.getText().toString().trim())) {
            input_city.setError(getString(R.string.enter_city));
            return false;
        } else if (!ValidateInputs.isValidNumber(input_postcode.getText().toString().trim())) {
            input_postcode.setError(getString(R.string.invalid_post_code));
            return false;
        } else {
            return true;
        }
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ACCESS_FINE_LOCATION_INTENT_ID: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //If permission granted show location dialog if APIClient is not null
                    locationTrack = new GPStracker(getActivity(), this);
                    if (!locationTrack.canGetLocation()) {
                        locationTrack.showSettingsAlert();
                    }

                } else {
                    Toast.makeText(getActivity(), "Location Permission denied.", Toast.LENGTH_SHORT).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }

    }

    @Override
    public void gotoSettings() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivityForResult(intent, 400);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 400 && resultCode == RESULT_OK) {
            locationTrack = new GPStracker(getActivity(), this);
            if (locationTrack.canGetLocation()) {
                Geocoder geocoder;
                List<android.location.Address> addresses;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                try {
                    addresses = geocoder.getFromLocation(locationTrack.getLatitude(), locationTrack.getLongitude(), 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                    String address = addresses.get(0).getAddressLine(0);
                    List<String> elephantList = Arrays.asList(address.split(","));
//                    address = elephantList.get(elephantList.size() - 4);
                    // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    String city = addresses.get(0).getLocality();
                    String country = addresses.get(0).getCountryName();
                    String state = addresses.get(0).getAdminArea();
                    String pincode = addresses.get(0).getPostalCode();
                    Log.d("ADDRESS", address + "\n" + city + "\n" + country + "\n" + state + "\n" + pincode);

                    shippingAddress_new = new AddressDetails();

                    shippingAddress_new.setCountryName(country);
                    shippingAddress_new.setZoneName(state);
                    shippingAddress_new.setCity(city);
                    shippingAddress_new.setStreet(address);
                    shippingAddress_new.setPostcode(pincode);
                    shippingAddress_new.setZoneId(182);
                    shippingAddress_new.setCountriesId(99);

                    setAddressDetail(shippingAddress_new);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }
}

