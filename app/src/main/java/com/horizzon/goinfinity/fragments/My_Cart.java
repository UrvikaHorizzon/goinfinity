package com.horizzon.goinfinity.fragments;


import androidx.annotation.Nullable;

import android.content.Context;
import android.content.DialogInterface;
import android.location.LocationManager;
import android.os.Bundle;
import android.content.Intent;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.goinfinity.activities.MainActivity;
import com.horizzon.goinfinity.R;

import java.util.ArrayList;
import java.util.List;

import com.horizzon.goinfinity.activities.Login;
import com.horizzon.goinfinity.activities.PaytmTest_First_Activity;
import com.horizzon.goinfinity.adapters.CartItemsAdapter;
import com.horizzon.goinfinity.constant.ConstantValues;
import com.horizzon.goinfinity.databases.User_Cart_DB;
import com.horizzon.goinfinity.models.cart_model.CartProduct;
import com.horizzon.goinfinity.models.product_model.ProductDetails;
import com.horizzon.goinfinity.network.APIClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class My_Cart extends Fragment {

    public TextView cart_total_price;
    String customerID;
    RecyclerView cart_items_recycler;
    LinearLayout cart_view, cart_view_empty;
    Button cart_checkout_btn, continue_shopping_btn;

    CartItemsAdapter cartItemsAdapter;
    User_Cart_DB user_cart_db = new User_Cart_DB();

    List<CartProduct> cartItemsList;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.my_cart, container, false);

        setHasOptionsMenu(true);

        // Enable Drawer Indicator with static variable actionBarDrawerToggle of MainActivity
        MainActivity.actionBarDrawerToggle.setDrawerIndicatorEnabled(false);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(getString(R.string.actionCart));

        // Get the List of Cart Items from the Local Databases User_Cart_DB



        // Binding Layout Views
        cart_view = (LinearLayout) rootView.findViewById(R.id.cart_view);
        cart_total_price = (TextView) rootView.findViewById(R.id.cart_total_price);
        cart_checkout_btn = (Button) rootView.findViewById(R.id.cart_checkout_btn);
        cart_items_recycler = (RecyclerView) rootView.findViewById(R.id.cart_items_recycler);
        cart_view_empty = (LinearLayout) rootView.findViewById(R.id.cart_view_empty);
        continue_shopping_btn = (Button) rootView.findViewById(R.id.continue_shopping_btn);
        customerID = this.getContext().getSharedPreferences("UserInfo", getContext().MODE_PRIVATE).getString("userID", "");


        cartItemsList=new ArrayList<>();
        cartItemsList = user_cart_db.getCartItems();

        // Change the Visibility of cart_view and cart_view_empty LinearLayout based on CartItemsList's Size
        if (cartItemsList.size() != 0) {
            cart_view.setVisibility(View.VISIBLE);
            cart_view_empty.setVisibility(View.GONE);

//            updateCartValue();

            Log.e("customer_id",customerID);
        } else {
            cart_view.setVisibility(View.GONE);
            cart_view_empty.setVisibility(View.VISIBLE);
        }



        setItemOnRecyclerview();



        // Handle Click event of continue_shopping_btn Button
        continue_shopping_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                
                Bundle bundle = new Bundle();
                bundle.putBoolean("isSubFragment", false);
    
                // Navigate to Products Fragment
                Fragment fragment = new Products();
                fragment.setArguments(bundle);
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager.beginTransaction()
                        .replace(R.id.main_fragment, fragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .addToBackStack(getString(R.string.actionCart)).commit();

            }
        });


        // Handle Click event of cart_checkout_btn Button
        cart_checkout_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Check if cartItemsList isn't empty
                if (cartItemsList.size() != 0) {

                    // Check if User is Logged-In
                    if (ConstantValues.IS_USER_LOGGED_IN) {

                        // Navigate to Shipping_Address Fragment

                        LocationManager lm = (LocationManager)getActivity().getSystemService(Context.LOCATION_SERVICE);
                        boolean gps_enabled = false;
                        boolean network_enabled = false;
                        try {
                            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
                        } catch(Exception ex) {}

                        try {
                            network_enabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
                        } catch(Exception ex) {}

                        if(!gps_enabled && !network_enabled) {
                            // notify user
                            new AlertDialog.Builder(getActivity())
                                    .setMessage(R.string.gps_network_not_enabled)
                                    .setPositiveButton(R.string.open_location_settings, new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                                            getContext().startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                                        }
                                    }).setNegativeButton(R.string.Cancel,null)
                                            .show();
                        }else {

                            Fragment fragment = new Shipping_Address();
                            FragmentManager fragmentManager = getFragmentManager();
                            fragmentManager.beginTransaction().replace(R.id.main_fragment, fragment)
                                    .addToBackStack(getString(R.string.actionCart)).commit();



                        }

                    } else {
                        // Navigate to Login Activity
                        Intent i = new Intent(getContext(), PaytmTest_First_Activity.class);
                        getContext().startActivity(i);
                        ((MainActivity) getContext()).finish();
                        ((MainActivity) getContext()).overridePendingTransition(R.anim.enter_from_left, R.anim.exit_out_left);
                    }
                }
            }
        });


        return rootView;
    }

    private void setItemOnRecyclerview() {
        // Initialize the AddressListAdapter for RecyclerView
        cartItemsList=new ArrayList<>();
        cartItemsList = user_cart_db.getCartItems();
        cartItemsAdapter = new CartItemsAdapter(getContext(), cartItemsList, My_Cart.this);

        // Set the Adapter and LayoutManager to the RecyclerView
        cart_items_recycler.setAdapter(cartItemsAdapter);
        cart_items_recycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));


        // Show the Cart's Total Price with the help of static method of CartItemsAdapter
        cartItemsAdapter.setCartTotal();


        cartItemsAdapter.notifyDataSetChanged();
    }

    private void updateCartValue() {
        Call<CartProduct>call= APIClient.getInstance().getUpdateProduct(customerID);

        call.enqueue(new Callback<CartProduct>() {
            @Override
            public void onResponse(Call<CartProduct> call, Response<CartProduct> response) {

                if (response.body().getSuccess()!=null){
                if (response.body().getSuccess().equalsIgnoreCase("1")) {

                    List<ProductDetails> list = response.body().getData();

                    int actual_products_id = 0;
                    for (int i = 0; i < list.size(); i++) {
                        actual_products_id = user_cart_db.searchProduct(list.get(i).getProductsId());
                        Log.e("suceess", String.valueOf(actual_products_id));

                        if (list.get(i).getProductsId() == actual_products_id) {


//                            float updated_product_price_float = Float.parseFloat(list.get(i).getProductsPrice());
//                            int updatedprice = (int) Math.round(updated_product_price_float);


                            CartProduct cart = GetCartProduct(actual_products_id);
                            ProductDetails details = cart.getCustomersBasketProduct();

                            int total_quantity = details.getCustomersBasketQuantity();


                            float actual_product_price_float = Float.parseFloat(details.getProductsPrice());
                            int actual_product_price = (int) Math.round(actual_product_price_float);


                            float actual_total_price_float = Float.parseFloat(details.getTotalPrice());
                            int actual_total_price = (int) Math.round(actual_total_price_float);


                            double actual_price = actual_product_price * total_quantity;
                            double othercharges = actual_total_price - actual_price;


                            Log.e("quantity", String.valueOf(details.getCustomersBasketQuantity()));

//                            if (updatedprice > actual_product_price) {
//                                double new_price = updatedprice * total_quantity;
//
//                                double total_updated_price = new_price + othercharges;
//
//
//                                ProductDetails data = new ProductDetails();
//                                data.setProductsId(list.get(i).getProductsId());
//                                data.setProductsPrice(String.valueOf(updatedprice));
//                                data.setCustomersBasketQuantity(total_quantity);
//                                data.setTotalPrice(String.valueOf(total_updated_price));
////
//                                int update = user_cart_db.updateCartQuantity(data);
//                                Log.e("update", String.valueOf(update));
////
//                                setItemOnRecyclerview();
//                            }

                        }

                    }

                }


                }
            }

            @Override
            public void onFailure(Call<CartProduct> call, Throwable t) {

            }
        });
    }


    //*********** Change the Layout View of My_Cart Fragment based on Cart Items ********//

    public void updateCartView(int cartListSize) {

        // Check if Cart has some Items
        if (cartListSize != 0) {
            cart_view.setVisibility(View.VISIBLE);
            cart_view_empty.setVisibility(View.GONE);
        } else {
            cart_view.setVisibility(View.GONE);
            cart_view_empty.setVisibility(View.VISIBLE);
        }
    }



    //*********** Static method to Add the given Item to User's Cart ********//

    public static void AddCartItem(CartProduct cartProduct) {
        User_Cart_DB user_cart_db = new User_Cart_DB();
        user_cart_db.addCartItem(cartProduct);
    }



    //*********** Static method to Get the Cart Product based on product_id ********//

    public static CartProduct GetCartProduct(int product_id) {
        User_Cart_DB user_cart_db = new User_Cart_DB();

        CartProduct cartProduct = user_cart_db.getCartProduct
                (
                        product_id
                );

        return cartProduct;
    }



    //*********** Static method to Update the given Item in User's Cart ********//

    public static void UpdateCartItem(CartProduct cartProduct) {
        User_Cart_DB user_cart_db = new User_Cart_DB();
        user_cart_db.updateCartItem
                (
                        cartProduct
                );
    }



    //*********** Static method to Delete the given Item from User's Cart ********//

    public static void DeleteCartItem(int cart_item_id) {
        User_Cart_DB user_cart_db = new User_Cart_DB();
        user_cart_db.deleteCartItem
                (
                        cart_item_id
                );
    }



    //*********** Static method to Clear User's Cart ********//

    public static void ClearCart() {
        User_Cart_DB user_cart_db = new User_Cart_DB();
        user_cart_db.clearCart();
    }



    //*********** Static method to get total number of Items in User's Cart ********//

    public static int getCartSize() {
        int cartSize = 0;
        
        User_Cart_DB user_cart_db = new User_Cart_DB();
        List<CartProduct> cartItems = user_cart_db.getCartItems();
        
        for (int i=0;  i<cartItems.size();  i++) {
            cartSize += cartItems.get(i).getCustomersBasketProduct().getCustomersBasketQuantity();
        }
        
        return cartSize;
    }


    //*********** Static method to check if the given Product is already in User's Cart ********//

    public static boolean checkCartHasProduct(int cart_item_id) {
        User_Cart_DB user_cart_db = new User_Cart_DB();
        return user_cart_db.getCartItemsIDs().contains(cart_item_id);
    }



    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        // Hide Cart Icon in the Toolbar
        MenuItem cartItem = menu.findItem(R.id.toolbar_ic_cart);
        MenuItem searchItem = menu.findItem(R.id.toolbar_ic_search);
        cartItem.setVisible(false);
        searchItem.setVisible(true);
    }
    
}

