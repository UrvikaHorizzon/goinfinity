package com.horizzon.goinfinity.network;


import com.google.gson.JsonArray;
import com.google.gson.annotations.Expose;
import com.horizzon.goinfinity.models.address_model.AddressData;
import com.horizzon.goinfinity.models.address_model.Countries;
import com.horizzon.goinfinity.models.address_model.Zones;
import com.horizzon.goinfinity.models.banner_model.BannerData;
import com.horizzon.goinfinity.models.cart_model.CartProduct;
import com.horizzon.goinfinity.models.category_model.CategoryData;
import com.horizzon.goinfinity.models.check_user_model.checkuser_response_model;
import com.horizzon.goinfinity.models.contact_model.ContactUsData;
import com.horizzon.goinfinity.models.coupons_model.CouponsInfo;
import com.horizzon.goinfinity.models.device_model.AppSettingsData;
import com.horizzon.goinfinity.models.filter_model.get_filters.FilterData;
import com.horizzon.goinfinity.models.language_model.LanguageData;
import com.horizzon.goinfinity.models.logindatamodel.loginresponsemodel;
import com.horizzon.goinfinity.models.news_model.all_news.NewsData;
import com.horizzon.goinfinity.models.news_model.news_categories.NewsCategoryData;
import com.horizzon.goinfinity.models.notifactionModel.NotificationModel;
import com.horizzon.goinfinity.models.pages_model.PagesData;
import com.horizzon.goinfinity.models.paytm_model;
import com.horizzon.goinfinity.models.paytm_new;
import com.horizzon.goinfinity.models.product_model.GetAllProducts;
import com.horizzon.goinfinity.models.coupons_model.CouponsData;
import com.horizzon.goinfinity.models.payment_model.PaymentMethodsData;
import com.horizzon.goinfinity.models.promolist_model.CouponResponse;
import com.horizzon.goinfinity.models.promolist_model.promo_response_model;
import com.horizzon.goinfinity.models.shipping_model.PostTaxAndShippingData;
import com.horizzon.goinfinity.models.order_model.OrderData;
import com.horizzon.goinfinity.models.payment_model.GetBrainTreeToken;
import com.horizzon.goinfinity.models.order_model.PostOrder;
import com.horizzon.goinfinity.models.product_model.ProductData;
import com.horizzon.goinfinity.models.search_model.SearchData;
import com.horizzon.goinfinity.models.shipping_model.ShippingRateData;
import com.horizzon.goinfinity.models.shippingmodel.shippingdatamodel;
import com.horizzon.goinfinity.models.user_model.DistributorData;
import com.horizzon.goinfinity.models.user_model.UserData;
import com.horizzon.goinfinity.models.verifyModel;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.POST;


/**
 * APIRequests contains all the Network Request Methods with relevant API Endpoints
 **/

public interface APIRequests {


    //******************** User Data ********************//

    @FormUrlEncoded
    @POST("processRegistration")
    Call<UserData> processRegistration(@Field("customers_firstname") String customers_firstname,
                                       @Field("customers_lastname") String customers_lastname,
                                       @Field("customers_email_address") String customers_email_address,
                                       @Field("customers_telephone") String customers_telephone,
                                       @Field("customers_picture") String customers_picture);

    @FormUrlEncoded
    @POST("processLogin")
    Call<UserData> processLogin(@Field("customers_email_address") String customers_email_address,
                                @Field("customers_password") String customers_password);

    @FormUrlEncoded
    @POST("facebookRegistration")
    Call<UserData> facebookRegistration(@Field("access_token") String access_token);

    @FormUrlEncoded
    @POST("googleRegistration")
    Call<UserData> googleRegistration(@Field("idToken") String idToken,
                                      @Field("userId") String userId,
                                      @Field("givenName") String givenName,
                                      @Field("familyName") String familyName,
                                      @Field("email") String email,
                                      @Field("imageUrl") String imageUrl);


    @FormUrlEncoded
    @POST("processForgotPassword")
    Call<UserData> processForgotPassword(@Field("customers_email_address") String customers_email_address);

    @FormUrlEncoded
    @POST("updateCustomerInfo")
    Call<UserData> updateCustomerInfo(@Field("customers_id") String customers_id,
                                      @Field("customers_firstname") String customers_firstname,
                                      @Field("customers_lastname") String customers_lastname,
                                      @Field("customers_telephone") String customers_telephone,
                                      @Field("customers_dob") String customers_dob,
                                      @Field("customers_picture") String customers_picture,
                                      @Field("customers_old_picture") String customers_old_picture,
                                      @Field("customers_password") String customers_password);


    @FormUrlEncoded
    @POST("new_dealer")
    Call<ContactUsData> addNewDealer(@Field("first_name") String first_name,
                                     @Field("last_name") String last_name,
                                     @Field("user_id") String user_id,
                                     @Field("distributor_name") String distributor);


    //******************** Address Data ********************//

    @POST("getCountries")
    Call<Countries> getCountries();

    @FormUrlEncoded
    @POST("getZones")
    Call<Zones> getZones(@Field("zone_country_id") String zone_country_id);

    @FormUrlEncoded
    @POST("getAllAddress")
    Call<AddressData> getAllAddress(@Field("customers_id") String customers_id);

    @FormUrlEncoded
    @POST("addShippingAddress")
    Call<AddressData> addUserAddress(@Field("customers_id") String customers_id,
                                     @Field("entry_firstname") String entry_firstname,
                                     @Field("entry_lastname") String entry_lastname,
                                     @Field("entry_street_address") String entry_street_address,
                                     @Field("entry_postcode") String entry_postcode,
                                     @Field("entry_city") String entry_city,
                                     @Field("entry_country_id") String entry_country_id,
                                     @Field("entry_zone_id") String entry_zone_id,
                                     @Field("customers_default_address_id") String customers_default_address_id);

    @FormUrlEncoded
    @POST("updateShippingAddress")
    Call<AddressData> updateUserAddress(@Field("customers_id") String customers_id,
                                        @Field("address_id") String address_id,
                                        @Field("entry_firstname") String entry_firstname,
                                        @Field("entry_lastname") String entry_lastname,
                                        @Field("entry_street_address") String entry_street_address,
                                        @Field("entry_postcode") String entry_postcode,
                                        @Field("entry_city") String entry_city,
                                        @Field("entry_country_id") String entry_country_id,
                                        @Field("entry_zone_id") String entry_zone_id,
                                        @Field("customers_default_address_id") String customers_default_address_id);

    @FormUrlEncoded
    @POST("updateDefaultAddress")
    Call<AddressData> updateDefaultAddress(@Field("customers_id") String customers_id,
                                           @Field("address_book_id") String address_book_id);

    @FormUrlEncoded
    @POST("deleteShippingAddress")
    Call<AddressData> deleteUserAddress(@Field("customers_id") String customers_id,
                                        @Field("address_book_id") String address_book_id);


    //******************** Category Data ********************//

    @FormUrlEncoded
    @POST("allCategories")
    Call<CategoryData> getAllCategories(@Field("language_id") int language_id);


    //******************** Product Data ********************//

    @POST("getAllProducts")
    Call<ProductData> getAllProducts(@Body GetAllProducts getAllProducts);

    @FormUrlEncoded
    @POST("pricedata")
    Call<JsonArray> getPrice(@Field("customers_id") String customers_id,
                             @Field("product_id") String product_id);

    @FormUrlEncoded
    @POST("distributor")
    Call<DistributorData> getFLname(@Field("id") String customers_id);


    @FormUrlEncoded
    @POST("likeProduct")
    Call<ProductData> likeProduct(@Field("liked_products_id") int liked_products_id,
                                  @Field("liked_customers_id") String liked_customers_id);

    @FormUrlEncoded
    @POST("unlikeProduct")
    Call<ProductData> unlikeProduct(@Field("liked_products_id") int liked_products_id,
                                    @Field("liked_customers_id") String liked_customers_id);


    @FormUrlEncoded
    @POST("getFilters")
    Call<FilterData> getFilters(@Field("categories_id") int categories_id,
                                @Field("language_id") int language_id);


    @FormUrlEncoded
    @POST("getSearchData")
    Call<SearchData> getSearchData(@Field("searchValue") String searchValue,
                                   @Field("language_id") int language_id);


    //******************** News Data ********************//

    @FormUrlEncoded
    @POST("getAllNews")
    Call<NewsData> getAllNews(@Field("language_id") int language_id,
                              @Field("page_number") int page_number,
                              @Field("is_feature") int is_feature,
                              @Field("categories_id") String categories_id);

    @FormUrlEncoded
    @POST("allNewsCategories")
    Call<NewsCategoryData> allNewsCategories(@Field("language_id") int language_id,
                                             @Field("page_number") int page_number);


    //******************** Order Data ********************//

    @POST("addToOrder")
    Call<OrderData> addToOrder(@Body PostOrder postOrder);

    @FormUrlEncoded
    @POST("notifyDistributor")
    Call<NotificationModel> sendDistrubutedDId(@Field("distributorId") String distributorId,
                                               @Field("message") String message,
                                               @Field("nTitle") String nTitle);

    @POST("getOrderResponse")
    Call<OrderData> addToPaytmOrder(@Body PostOrder postOrder);

    @FormUrlEncoded
    @POST("getOrders")
    Call<OrderData> getOrders(@Field("customers_id") String customers_id,
                              @Field("language_id") int language_id);


    @FormUrlEncoded
    @POST("getCoupon")
    Call<CouponsData> getCouponInfo(@Field("code") String code);


    @GET("getPaymentMethods")
    Call<PaymentMethodsData> getPaymentMethods();

    @GET("generateBraintreeToken")
    Call<GetBrainTreeToken> generateBraintreeToken();


    //******************** Banner Data ********************//

    @GET("getBanners")
    Call<BannerData> getBanners();

    @GET("getfrontBanners")
    Call<BannerData> getfrontBanners();


    //******************** Tax & Shipping Data ********************//

    @POST("getRate")
    Call<ShippingRateData> getShippingMethodsAndTax(
            @Body PostTaxAndShippingData postTaxAndShippingData);


    //******************** Contact Us Data ********************//

    @FormUrlEncoded
    @POST("contactUs")
    Call<ContactUsData> contactUs(@Field("name") String name,
                                  @Field("email") String email,
                                  @Field("message") String message);


    //******************** Languages Data ********************//

    @GET("getLanguages")
    Call<LanguageData> getLanguages();


    //******************** App Settings Data ********************//

    @GET("siteSetting")
    Call<AppSettingsData> getAppSetting();


    //******************** Static Pages Data ********************//

    @FormUrlEncoded
    @POST("getAllPages")
    Call<PagesData> getStaticPages(@Field("language_id") int language_id);


    //******************** Notifications Data ********************//

    @FormUrlEncoded
    @POST("registerDevices")
    Call<UserData> registerDeviceToFCM(@Field("device_id") String device_id,
                                       @Field("device_type") String device_type,
                                       @Field("ram") String ram,
                                       @Field("processor") String processor,
                                       @Field("device_os") String device_os,
                                       @Field("location") String location,
                                       @Field("device_model") String device_model,
                                       @Field("manufacturer") String manufacturer,
                                       @Field("customers_id") String customers_id);


    @FormUrlEncoded
    @POST("notify_me")
    Call<ContactUsData> notify_me(@Field("is_notify") String is_notify,
                                  @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("createChecksum")
    Call<paytm_model> getChecksum(@Field("MID") String mid, @Field("ORDER_ID") String order_id, @Field("CUS_ID") String cust_id, @Field("INDUSTRY_TYPE_ID") String IND_id, @Field("TXN_AMOUNT") String price, @Field("WEBSITE") String website, @Field("CHANNEL_ID") String channel_id);


    @FormUrlEncoded
    @POST("checksumVerify")
    Call<verifyModel> verifychecksum(@Field("MID") String mid, @Field("ORDER_ID") String order_id, @Field("CUS_ID") String cust_id, @Field("INDUSTRY_TYPE_ID") String IND_id, @Field("TXN_AMOUNT") String price, @Field("WEBSITE") String website, @Field("CHECKSUMHASH") String callbackurl);

    @FormUrlEncoded
    @POST("http://goinfinity.in/paytm/generateChecksum.php")
    Call<paytm_new> getChecksumnew(@Field("MID") String mid, @Field("ORDER_ID") String order_id, @Field("CUS_ID") String cust_id, @Field("INDUSTRY_TYPE_ID") String IND_id, @Field("TXN_AMOUNT") String price, @Field("WEBSITE") String website, @Field("CHANNEL_ID") String channel_id);


    @FormUrlEncoded
    @POST("product_price")
    Call<CartProduct> getUpdateProduct(@Field("id") String customer_id);

    @GET("http://goinfinity.in/getCouponcode")
    Call<CouponsData> getpromocodelist();


    @GET("http://goinfinity.in/getCouponcode")
    Call<CouponResponse> getPromo();

    @GET("getShipping")
    Call<shippingdatamodel> getShippingcharges();

    @GET("getCouponcode")
    Call<CouponsInfo> getCoupanlist();


    @FormUrlEncoded
    @POST("customer_information")
    Call<loginresponsemodel> getUserData(@Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST("get_customer_coupon")
    Call<promo_response_model> getCouponStatus(@Field("customers_id") String customer_id);

    @FormUrlEncoded
    @POST("usercheak")
    Call<checkuser_response_model>checkUserType(@Field("id")String user_id);
}

