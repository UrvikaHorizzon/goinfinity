package com.horizzon.goinfinity.network;

import android.content.Context;
import android.util.Log;

import com.horizzon.goinfinity.R;

import java.io.IOException;

import com.horizzon.goinfinity.app.App;
import com.horizzon.goinfinity.constant.ConstantValues;
import com.horizzon.goinfinity.models.banner_model.BannerData;
import com.horizzon.goinfinity.models.category_model.CategoryData;
import com.horizzon.goinfinity.models.device_model.AppSettingsData;
import com.horizzon.goinfinity.models.pages_model.PagesData;
import com.horizzon.goinfinity.models.pages_model.PagesDetails;

import retrofit2.Call;


/**
 * StartAppRequests contains some Methods and API Requests, that are Executed on Application Startup
 **/

public class StartAppRequests {

    private App app = new App();


    public StartAppRequests(Context context) {
        app = ((App) context.getApplicationContext());
    }


    //*********** Contains all methods to Execute on Startup ********//

    public void StartRequests() {

        RequestBanners();
        RequestAppSetting();
        RequestAllCategories();
        RequestStaticPagesData();
    }


    //*********** API Request Method to Fetch App Banners ********//

    private void RequestBanners() {



        try {

            Call<BannerData> call = APIClient.getInstance()
                    .getBanners();

            BannerData bannerData = new BannerData();
            bannerData = call.execute().body();

            if (!bannerData.getSuccess().isEmpty())
                app.setBannersList(bannerData.getData());

        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    //*********** API Request Method to Fetch All Categories ********//

    private void RequestAllCategories() {

        Call<CategoryData> call = APIClient.getInstance()
                .getAllCategories
                        (
                                ConstantValues.LANGUAGE_ID
                        );

        CategoryData categoryData = new CategoryData();

        try {
            categoryData = call.execute().body();
            if (!categoryData.getSuccess().isEmpty())
                app.setCategoriesList(categoryData.getData());

        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    //*********** Request App Settings from the Server ********//

    private void RequestAppSetting() {

        Call<AppSettingsData> call = APIClient.getInstance()
                .getAppSetting();

        AppSettingsData appSettingsData = null;

        try {
            appSettingsData = call.execute().body();

            if (!appSettingsData.getSuccess().isEmpty())
                app.setAppSettingsDetails(appSettingsData.getProductData().get(0));

        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    //*********** Request Static Pages Data from the Server ********//

    private void RequestStaticPagesData() {

        ConstantValues.ABOUT_US = app.getString(R.string.lorem_ipsum);
        ConstantValues.TERMS_SERVICES = app.getString(R.string.lorem_ipsum);
        ConstantValues.PRIVACY_POLICY = app.getString(R.string.lorem_ipsum);
        ConstantValues.REFUND_POLICY = app.getString(R.string.lorem_ipsum);


        Call<PagesData> call = APIClient.getInstance()
                .getStaticPages
                        (
                                ConstantValues.LANGUAGE_ID
                        );

        PagesData pages = new PagesData();

        try {
            pages = call.execute().body();

            if (pages.getSuccess().equalsIgnoreCase("1")) {

                app.setStaticPagesDetails(pages.getPagesData());

                for (int i = 0; i < pages.getPagesData().size(); i++) {
                    PagesDetails page = pages.getPagesData().get(i);

                    if (page.getSlug().equalsIgnoreCase("about-us")) {
                        ConstantValues.ABOUT_US = page.getDescription();
                    } else if (page.getSlug().equalsIgnoreCase("term-conditions")) {
                        ConstantValues.TERMS_SERVICES = page.getDescription();
                    } else if (page.getSlug().equalsIgnoreCase("privacy-policy")) {
                        ConstantValues.PRIVACY_POLICY = page.getDescription();
                    } else if (page.getSlug().equalsIgnoreCase("refund-policy")) {
                        ConstantValues.REFUND_POLICY = page.getDescription();
                    }
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }

}
